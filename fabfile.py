# -*- coding: utf-8 -*-
# fabfile for Django:
from fabric.api import *
from core.utils.fabfile import *


# globals
env.git_host = 'git.tkplaneta.com'
env.project_name = 'tkplaneta'
env.venv_name = 'tkplaneta-se'
env.django_settings_module = 'tkplaneta.settings.dev'
env.repo = 'git@{git_host}:/projects/{project_name}-se'.format(**env)
env.use_ssh_config = env.remote_deployment
env.shared_dirs = 'config media/uploads media/sitemaps static releases/{current,previous}'


@task
def dev():
    """Development server"""
    env.venv_name = 'tkplaneta-se_stage'
    env.user = 'deploy'
    env.branch = 'master'
    env.django_settings_module = 'tkplaneta.settings.stage'
    env.hosts = ['deploy@tkplaneta.com']
    env.vhosts_root = '/var/www/vhosts'
    env.host_name = 'stage.svarozhichi.ua'
    env.vhost_path = '{vhosts_root}/{host_name}'.format(**env)
    env.release_path = '{vhosts_root}/{host_name}/releases/current'.format(**env)
    env.touch_reload = '{release_path}/{project_name}/wsgi_stage.py'.format(**env)


@task
def prod():
    """Production server"""
    env.user = 'deploy'
    env.branch = 'master'
    env.django_settings_module = 'tkplaneta.settings.prod'
    env.hosts = ['deploy@tkplaneta.com']
    env.host_name = 'svarozhichi.ua'
    env.vhosts_root = '/var/www/vhosts'
    env.vhost_path = '{vhosts_root}/{host_name}'.format(**env)
    env.release_path = '{vhosts_root}/{host_name}/releases/current'.format(**env)
    env.touch_reload = '{release_path}/{project_name}/wsgi.py'.format(**env)
