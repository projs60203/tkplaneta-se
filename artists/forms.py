# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django.utils.translation import ugettext_lazy as _

from dal import autocomplete

from artists.models import Composition


class CompositionAdminForm(forms.ModelForm):
    class Meta:
        model = Composition
        fields = '__all__'
        widgets = {
            'artist': autocomplete.ModelSelect2(
                url='artist-autocomplete',
                attrs={
                    'data-placeholder': _('Select Artist'),
                    'class': 'artist-select',
                    # 'data-minimum-input-length': 2,
                },
            ),
        }

    def __init__(self, *args, **kwargs):
        super(CompositionAdminForm, self).__init__(*args, **kwargs)
        self.fields['artist'].empty_label = ''
