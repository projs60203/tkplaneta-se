# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _


@python_2_unicode_compatible
class Artist(models.Model):
    enabled = models.BooleanField(_('enabled'), default=True)
    name = models.CharField(_('name'), max_length=255)

    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('updated at'), auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
        verbose_name = _('artist')
        verbose_name_plural = _('artists')


@python_2_unicode_compatible
class Composition(models.Model):
    artist = models.ForeignKey(Artist, verbose_name=_('artist'), related_name='compositions')

    enabled = models.BooleanField(_('enabled'), default=True)
    name = models.CharField(_('name'), max_length=255)

    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('updated at'), auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
        verbose_name = _('composition')
        verbose_name_plural = _('compositions')
