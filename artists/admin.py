# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from dal_admin_filters import AutocompleteFilter

from artists.models import Artist, Composition
from artists.forms import CompositionAdminForm


class ArtistFilter(AutocompleteFilter):
    title = _('artist')                   # filter's title
    field_name = 'artist'                 # field name - ForeignKey to State model
    autocomplete_url = 'artist-autocomplete'  # url name of State autocomplete view


class CompositionInline(admin.TabularInline):
    model = Composition
    readonly_fields = ('created_at', 'updated_at')
    extra = 0


class ArtistAdmin(admin.ModelAdmin):
    list_display = ('name', 'enabled', 'created_at', 'updated_at')
    list_editable = ('enabled',)
    search_fields = ('name',)
    readonly_fields = ('created_at', 'updated_at')
    inlines = [CompositionInline]


class CompositionAdmin(admin.ModelAdmin):
    list_display = ('name', 'artist', 'enabled', 'created_at', 'updated_at')
    list_editable = ('enabled',)
    search_fields = ('name', 'artist__name')
    readonly_fields = ('created_at', 'updated_at')
    list_filter = (ArtistFilter,)
    form = CompositionAdminForm

    class Media:    # Empty media class is required if you are using autocomplete filter
        pass        # If you know better solution for altering admin.media from filter instance
                    #   - please contact me or make a pull request


admin.site.register(Artist, ArtistAdmin)
admin.site.register(Composition, CompositionAdmin)

