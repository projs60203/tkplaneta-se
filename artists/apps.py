# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class ArtistsConfig(AppConfig):
    name = 'artists'
    verbose_name = _('Artists')
