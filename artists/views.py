# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from dal import autocomplete

from artists.models import Artist, Composition


class ArtistAutocomplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        # if not self.request.user.is_authenticated():
        #     return Artist.objects.none()

        qs = Artist.objects.filter(enabled=True)

        composition = self.forwarded.get('composition', None)
        if composition:
            qs = qs.filter(compositions__in=[composition])

        if self.q:
            qs = qs.filter(name__icontains=self.q)

        return qs


class CompositionAutocomplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        # if not self.request.user.is_authenticated():
        #     return Composition.objects.none()

        qs = Composition.objects.filter(enabled=True)

        artist = self.forwarded.get('artist', None)
        if artist:
            qs = qs.filter(artist=artist)

        if self.q:
            qs = qs.filter(name__icontains=self.q)

        return qs
