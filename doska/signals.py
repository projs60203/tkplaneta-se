# -*- coding: utf-8 -*-
from __future__ import unicode_literals


def advert_post_save(sender, **kwargs):
    instance = kwargs.get('instance', None)
    # print 'enter in signal!!!!!!!!!'
    if instance and instance.category_id:
        instance.category.update_conters()

def advert_post_delete(sender, **kwargs):
    instance = kwargs.get('instance', None)
    # print 'enter in signal!!!!!!!!!'
    if instance and instance.category_id:
        instance.category.update_conters()

