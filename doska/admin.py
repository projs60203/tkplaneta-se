# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from adminsortable.admin import SortableAdmin

from doska.models import Category, Advert, Gallery, AdditionalPage
from doska.forms import AdvertAdminForm


class GalleryInline(admin.StackedInline):
    model = Gallery
    extra = 0


class AdditionalPageInline(admin.StackedInline):
    model = AdditionalPage
    extra = 0

class CategoryAdmin(SortableAdmin):
    readonly_fields = (
        'buys_count', 'sells_count', 'created_at', 'updated_at'
    )
    list_display = ('name', 'display_order', 'created_at', 'updated_at')
    list_editable = ('display_order',)
    fieldsets = (
        (None, {'fields': (
            'name', 'slug'
        )}),
        (_('Meta Tags'), {'fields': ('meta_title', 'meta_description', 'meta_keywords'),
                          'classes': ('collapse',)}),
        (_('Advanced Settings'), {
            'classes': ('collapse',),
            'fields': (
                'buys_count',
                'sells_count',
                'created_at',
                'updated_at',
            )
        }),
    )
admin.site.register(Category, CategoryAdmin)


class AdvertAdmin(admin.ModelAdmin):
    readonly_fields = (
        'created_at', 'updated_at'
    )
    search_fields = ('pk', 'title', 'title2', 'content')
    list_display = ('pk', 'admin_thumbnail', 'title', 'title2', 'ad_type', 'category', 'enabled', 'date', 'created_at', 'updated_at')
    list_display_links = ('pk', 'admin_thumbnail', 'title')
    list_editable = ('enabled',)
    list_filter = ('ad_type', 'category', 'currency', 'date', 'created_at', 'updated_at')
    fieldsets = (
        (None, {'fields': (
            'enabled', 'ad_type', 'category', 'title', 'title2', 'date', 'image', 'price', 'currency', 'negotiable', 'content'
        )}),
        (_('Meta Tags'), {'fields': ('meta_title', 'meta_description', 'meta_keywords'),
                          'classes': ('collapse',)}),
        (_('Advanced Settings'), {
            'classes': ('collapse',),
            'fields': (
                'created_at',
                'updated_at',
            )
        }),
    )
    inlines = [AdditionalPageInline, GalleryInline]
    form = AdvertAdminForm
admin.site.register(Advert, AdvertAdmin)
