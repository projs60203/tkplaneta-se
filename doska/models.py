# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.conf import settings
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.db.models import signals


from adminsortable.models import SortableMixin
from ckeditor.fields import RichTextField
from filer.fields.image import FilerImageField
from easy_thumbnails.files import get_thumbnailer
from uuslug import uuslug

from core.utils.models.meta_tags import MetaAbstract
from doska.signals import advert_post_save, advert_post_delete


class ADVERT_TYPE:
    BUY, SELL = ('buy', 'sell')
    CHOICES = (
        (BUY, _('Buy')),
        (SELL, _('Sell')),
    )


@python_2_unicode_compatible
class Category(SortableMixin, MetaAbstract):
    display_order = models.IntegerField(_('display order'), default=0)
    name = models.CharField(_('category name'), max_length=255)
    slug=models.SlugField(
        _('slug'),
        blank=True,
        default='',
        help_text=_('Provide a “slug” or leave blank for an automatically '
                    'generated one.'),
        max_length=255,
    )

    buys_count = models.PositiveIntegerField(_('buys count'), default=0, null=True)
    sells_count = models.PositiveIntegerField(_('sells count'), default=0, null=True)

    created_at = models.DateTimeField(_('created at'), auto_now_add=True, null=True)
    updated_at = models.DateTimeField(_('updated at'), auto_now=True, null=True)

    _metadata = {
        'title': 'meta_title',
        'description': 'meta_description',
        'keywords': 'keywords_prepare',
    }

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = uuslug(self.name, instance=self)
        super(Category, self).save(*args, **kwargs)

    def update_conters(self):
        self.buys_count = Advert.objects.filter(ad_type=ADVERT_TYPE.BUY).filter(enabled=True).filter(category_id=self.pk).count()
        self.sells_count = Advert.objects.filter(ad_type=ADVERT_TYPE.SELL).filter(enabled=True).filter(category_id=self.pk).count()
        self.save()

    class Meta:
        verbose_name = _('category')
        verbose_name_plural = _('categories')
        ordering = ['display_order']


@python_2_unicode_compatible
class Advert(MetaAbstract):
    enabled = models.BooleanField(_("enabled?"), default=True)
    ad_type = models.CharField(_('Advert type'), max_length=32, blank=True, default=ADVERT_TYPE.SELL, choices=ADVERT_TYPE.CHOICES, db_index=True)
    category = models.ForeignKey('doska.Category', verbose_name=_('category'), related_name='adverts', on_delete=models.DO_NOTHING)
    title = models.CharField(_('title 1'), max_length=255, default='')
    title2 = models.CharField(_('title 2'), max_length=255, default='')
    date = models.DateTimeField(_('date'), default=timezone.now)

    price = models.PositiveIntegerField(_("price"), default=0, blank=True)
    currency = models.ForeignKey('currencies.Currency', verbose_name=_('currency'), related_name='adverts', null=True, blank=True, on_delete=models.DO_NOTHING)
    negotiable = models.BooleanField(_('negotiable?'), default=False)

    image = FilerImageField(verbose_name=_("image"), default=None, null=True, blank=True,
        help_text=_('800x500'),
        on_delete=models.SET_NULL
    )

    content = RichTextField(_('content'), config_name='base', null=True, blank=True)

    created_at = models.DateTimeField(_('created at'), auto_now_add=True, editable=False, blank=True)
    updated_at = models.DateTimeField(_('updated at'), auto_now=True, editable=False, blank=True)

    _metadata = {
        'title': 'meta_title',
        'description': 'meta_description',
        'keywords': 'keywords_prepare',
        'image': 'image_url',
    }

    def __str__(self):
        return self.title

    def admin_thumbnail(self):
        if self.image:
            thumb_url = get_thumbnailer(self.image).get_thumbnail({'size': (100, 75)}).url
            return '<img src="{}" />'.format(thumb_url)
        else:
            return ''
    admin_thumbnail.short_description = _('image')
    admin_thumbnail.allow_tags = True

    def image_url(self):
        protocol = getattr(settings, 'META_SITE_PROTOCOL', 'http')
        domail = getattr(settings, 'META_SITE_DOMAIN', settings.ALLOWED_HOSTS[0])
        image = self.image.url
        return u'%s://%s%s' % (protocol, domail, image)

    class Meta:
        verbose_name = _('advert')
        verbose_name_plural = _('adverts')
        ordering = ['-date']


class Gallery(models.Model):
    advert = models.ForeignKey('doska.Advert', verbose_name=_('advert'), related_name='gallery', on_delete=models.CASCADE)
    image = FilerImageField(verbose_name=_("image"), default=None, null=True, blank=True,
        help_text=_('800x500'),
        on_delete=models.SET_NULL
    )

    def admin_thumbnail(self):
        if self.image:
            thumb_url = get_thumbnailer(self.image).get_thumbnail({'size': (100, 75)}).url
            return '<img src="{}" />'.format(thumb_url)
        else:
            return ''
    admin_thumbnail.short_description = _('image')
    admin_thumbnail.allow_tags = True

    class Meta:
        ordering = ['pk']
        verbose_name = _('gallery item')
        verbose_name_plural = _('gallery')


class AdditionalPage(models.Model):
    advert = models.ForeignKey('doska.Advert', verbose_name=_('advert'), related_name='additional_pages', on_delete=models.CASCADE)
    content = RichTextField(_('content'), config_name='base', null=True, blank=True)

    class Meta:
        ordering = ['pk']
        verbose_name = _('additional page')
        verbose_name_plural = _('additional pages')

# Advert
signals.post_save.connect(advert_post_save, sender=Advert)
signals.post_delete.connect(advert_post_delete, sender=Advert)
