# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.template import Library

from doska.models import ADVERT_TYPE

register = Library()

@register.filter
def category_name(category, ad_type):
    result = category.name
    if ad_type ==  ADVERT_TYPE.BUY:
        result = u'%s(%s)' % (category.name, category.buys_count)
    elif ad_type ==  ADVERT_TYPE.SELL:
        result = u'%s(%s)' % (category.name, category.sells_count)
    return result

@register.filter
def category_count(category, ad_type):
    result = 0
    if ad_type ==  ADVERT_TYPE.BUY:
        result = category.buys_count
    elif ad_type ==  ADVERT_TYPE.SELL:
        result = category.sells_count
    return result
