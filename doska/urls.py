# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url
from django.contrib import admin

from doska.views import view_category, view_type

admin.autodiscover()
urlpatterns = [
    url(r'^ad-type/(?P<ad_type>buy|sell)/$', view_type, name='advert-list-by-ad-type'),
    url(r'^ad-type/(?P<ad_type>buy|sell)/category/(?P<slug>[-\.\w\d]+)/$', view_category, name='advert-list-by-category'),
]
