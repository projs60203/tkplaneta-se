# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms

from currencies.models import Currency

class AdvertAdminForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(AdvertAdminForm, self).__init__(*args, **kwargs)
        self.fields['currency'].queryset = Currency.objects.filter(enabled=True)
