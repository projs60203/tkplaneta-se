# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render


from core.utils.pagination import paginator_factory
from doska.models import Category, Advert, Gallery


def get_param_name(ad_type):
    result = ''
    if ad_type == 'buy':
        result = 'buys_count'
    elif ad_type == 'sell':
        result = 'sells_count'
    return result

def get_categories(ad_type):
    categories = Category.objects.none()
    param_name = get_param_name(ad_type)
    if param_name:
        filter_key = '%s__gt' % param_name
        options = {filter_key: 0}
        categories = Category.objects.filter(**options)
    return categories

def view_type(request, ad_type):
    first_category = None
    categories = get_categories(ad_type)
    if categories:
        first_category = categories[0]
    if first_category:
        return view_category(request, ad_type, first_category.slug)
    else:
        return view_category(request, ad_type, '')

def view_category(request, ad_type, slug):
    template = 'doska/doska_category.html'
    categories = Category.objects.all()
    category = None
    try:
        category = categories.get(slug=slug)
    except categories.model.DoesNotExist:
        pass
    if category:
        adverts = category.adverts.order_by('pk').filter(ad_type=ad_type).filter(enabled=True)
    else:
        adverts = Category.objects.none()
    # per_page = 8
    # adverts = paginator_factory(request, adverts, int(per_page))
    # options = {
    #     'pages_start': 9,
    #     'pages_visible': 3,
    # }
    # pages_visible_negative = -options['pages_visible']
    # options['pages_visible_negative'] = pages_visible_negative
    # options['pages_visible_total'] = options['pages_visible'] + 1
    # options['pages_visible_total_negative'] = pages_visible_negative - 1
    ctx = {
        'ad_type': ad_type,
        'categories': categories,
        'current_category': category,
        'adverts': adverts,
        # 'is_paginated': adverts.has_other_pages(),
        'is_paginated': False,
        # 'paginator': adverts.paginator,
        'paginator': None,
        # 'pagination': options,
        # 'current_page': request.GET.get('page'),
        'current_page': None,
        'param_name': get_param_name(ad_type),
    }
    if ctx['current_category'] == None:
        ctx['meta'] = None
    else:
        ctx['meta'] = category.as_meta(request)
    return render(request, template, ctx)
