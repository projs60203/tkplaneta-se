# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _


from uuslug import uuslug
from filer.fields.image import FilerImageField
from filer.fields.file import FilerFileField
from easy_thumbnails.files import get_thumbnailer

@python_2_unicode_compatible
class VideoFile(models.Model):
    name = models.CharField(_('name'), max_length=255)
    slug = models.SlugField(_('Code name'), max_length=255,
                            blank=True,
                            help_text=_('If you do not fill in this field\'s'
                                        ' value will be generated automatically.'))

    mp4_file = FilerFileField(verbose_name=_('mp4'), null=True, blank=True, on_delete=models.SET_NULL, related_name='video_file_mp3')
    ogv_file = FilerFileField(verbose_name=_('ogv'), null=True, blank=True, on_delete=models.SET_NULL, related_name='video_file_ogv')
    webm_file = FilerFileField(verbose_name=_('webm'), null=True, blank=True, on_delete=models.SET_NULL, related_name='video_file_webm')

    image = FilerImageField(verbose_name=_('image'), null=True, blank=True, on_delete=models.SET_NULL, related_name='video_file_image')

    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('updated at'), auto_now=True)

    class Meta:
        verbose_name = _('video file')
        verbose_name_plural = _('video files')

    def __str__(self):
        return self.name

    def admin_thumbnail(self):
        if self.image:
            thumb_url = get_thumbnailer(self.image).get_thumbnail({'size': (155, 39), 'quality': 100}).url
            return '<img src="{}" />'.format(thumb_url)
        else:
            return ''
    admin_thumbnail.short_description = _('image')
    admin_thumbnail.allow_tags = True

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = uuslug(self.name, instance=self)
        super(VideoFile, self).save(*args, **kwargs)
