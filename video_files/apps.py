# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class VideoFilesConfig(AppConfig):
    name = 'video_files'
    verbose_name = _('Video files')
