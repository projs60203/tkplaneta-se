# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from video_files.models import VideoFile

class VideoFileAdmin(admin.ModelAdmin):
    readonly_fields = (
        'created_at', 'updated_at'
    )
    list_display = ('admin_thumbnail', 'name', 'slug', 'created_at', 'updated_at')
    list_display_links = ('admin_thumbnail', 'name')

    search_fields  = ('name',)

admin.site.register(VideoFile, VideoFileAdmin)
