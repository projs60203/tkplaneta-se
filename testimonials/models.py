# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from filer.fields.image import FilerImageField
from easy_thumbnails.files import get_thumbnailer


@python_2_unicode_compatible
class Testimonial(models.Model):
    enabled = models.BooleanField(_('enabled?'), default=True, blank=True)

    name = models.CharField(_('name'), max_length=30)
    image = FilerImageField(
        verbose_name=_('image'),
        help_text='804x414',
        on_delete=models.DO_NOTHING,
    )

    # first_name = models.CharField(_('first name'), max_length=30, null=True, blank=True)
    # last_name = models.CharField(_('last name'), max_length=30, null=True, blank=True)
    # patronymic = models.CharField(_('patronymic'), max_length=30, null=True, blank=True)
    # email = models.EmailField(_('email address'), null=True, blank=True)
    # phone = models.CharField(_('phone'), max_length=30, blank=True, null=True)
    #
    # content = HTMLField(_('content'))

    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('updated at'), auto_now=True)

    # @property
    # def full_name(self):
    #     """
    #     Returns the first_name plus the last_name, with a space in between.
    #     """
    #     full_name = ''
    #     if self.first_name and self.last_name and self.patronymic:
    #         full_name = '%s %s %s' % (self.last_name, self.first_name, self.patronymic)
    #     elif self.first_name and self.last_name:
    #         full_name = '%s %s' % (self.first_name, self.last_name)
    #     if self.first_name and self.patronymic:
    #         full_name = '%s %s' % (self.first_name, self.patronymic)
    #     elif self.first_name:
    #         full_name = self.first_name
    #     elif self.last_name:
    #         full_name = self.last_name
    #     return full_name.strip()

    @property
    def upload_dir(self):
        return "testimonials/pages"

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
        verbose_name = _('testimonial')
        verbose_name_plural = _('testimonials')

