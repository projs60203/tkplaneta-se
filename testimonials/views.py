# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from core.utils.pagination import paginator_factory
from testimonials.models import Testimonial


def testimonials_index(request):
    template = 'pages/testimonials.html'
    testimonials = Testimonial.objects.order_by('name').filter(enabled=True)
    testimonials = paginator_factory(request, testimonials, 1)
    ctx = {
        'testimonials': testimonials,
    }
    return render(request, template, ctx)

