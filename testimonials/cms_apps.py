# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


class TestimonialsApp(CMSApp):
    app_name = 'testimonials'
    name = _('Testimonials')

    def get_urls(self, page=None, language=None, **kwargs):
        return ['testimonials.urls']

apphook_pool.register(TestimonialsApp)