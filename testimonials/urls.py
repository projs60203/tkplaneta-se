# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url
from django.contrib import admin

from testimonials.views import testimonials_index

admin.autodiscover()

urlpatterns = [
    url(r'^$', testimonials_index, name='index'),
]
