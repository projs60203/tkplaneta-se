# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from testimonials.models import Testimonial

class TestimonialAdmin(admin.ModelAdmin):
    # list_display = ('last_name', 'first_name', 'patronymic', 'enabled', 'email', 'phone')
    # search_fields = ('first_name', 'last_name', 'patronymic', 'email', 'phone')
    # list_display_links = ('last_name', 'first_name', 'patronymic')
    list_display = ('name', 'enabled')
    readonly_fields = ('created_at', 'updated_at')
    list_editable = ('enabled',)

admin.site.register(Testimonial, TestimonialAdmin)
