# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url
from django.contrib import admin

from .views import certificate_order, certificate_thank_you

admin.autodiscover()

urlpatterns = [
    url(r'^$', certificate_order, name='certificate_order'),
    url(r'^thank-you/$', certificate_thank_you, name='certificate_thank_you'),
]
