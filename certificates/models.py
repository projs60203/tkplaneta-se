# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from filer.fields.image import FilerImageField
from easy_thumbnails.files import get_thumbnailer


class CERTIFICATE_STATUS:
    NEW = 'new'
    IN_WORK = 'in_work'
    PAID = 'paid'
    MADE = 'made'
    RENOUNCE = 'renounce'
    BLACK_LIST = 'black_list'

    CHOICES = (
        (NEW, _('new')),
        (IN_WORK, _('in work')),
        (PAID, _('paid')),
        (MADE, _('made')),
        (RENOUNCE, _('renounce')),
        (BLACK_LIST, _('black list'))
    )


class CERTIFICATE_PAYMENT_TYPE:
    CASH = 'cash'
    CREDIT_CARD = 'credit_card'

    CHOICES = (
        (CASH, _('Cash')),
        (CREDIT_CARD, _('Credit card')),
    )


@python_2_unicode_compatible
class Certificate(models.Model):
    created_at = models.DateTimeField(_('created at'), auto_now_add=True)

    name = models.CharField(_('name'), max_length=255)
    region = models.CharField(_('region'), max_length=255)
    city = models.CharField(_('locality'), max_length=255)
    phone = models.CharField(_('phone'), max_length=255, blank=True, default='')
    email = models.EmailField(_('email'), blank=True, default='')
    photo = FilerImageField(
        verbose_name=_('photo'),
        on_delete=models.DO_NOTHING,
    )

    payment = models.TextField(_('payment'), blank=True, null=True)
    order_number = models.CharField(_('order number'), max_length=255, blank=True, null=True)

    payment_type = models.CharField(_('payment type'), max_length=255,
        choices=CERTIFICATE_PAYMENT_TYPE.CHOICES, default=CERTIFICATE_PAYMENT_TYPE.CASH)
    price = models.FloatField(_('price'), blank=True, default=0.0)

    status = models.CharField(_('status'), max_length=255, choices=CERTIFICATE_STATUS.CHOICES, default=CERTIFICATE_STATUS.NEW)


    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('certificate')
        verbose_name_plural = _('certificates')

    def admin_thumbnail(self):
        if self.photo:
            thumb_url = get_thumbnailer(self.photo).get_thumbnail({'size': (50, 64), 'quality': 100}).url
            return '<img src="{}" />'.format(thumb_url)
        else:
            return ''
    admin_thumbnail.short_description = _('photo')
    admin_thumbnail.allow_tags = True
