# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from certificates.models import Certificate


class CertificateAdmin(admin.ModelAdmin):
    list_display = ('admin_thumbnail', 'name', 'status', 'created_at',)
    list_display_links = ('admin_thumbnail', 'name')
    readonly_fields = ('created_at',)
    list_filter = ('status', 'created_at',)
    fieldsets = (
        (
            None, {
                'fields': (
                    'created_at',
                    'name',
                    'region',
                    'city',
                    'phone',
                    'email',
                    'photo',
                    'payment',
                    'order_number',
                    'payment_type',
                    'price',
                    'status',
                )
            }
        ),
    )
admin.site.register(Certificate, CertificateAdmin)
