# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
import json
import uuid

from django.shortcuts import redirect, render
from django.http import HttpResponse, JsonResponse
from django.contrib.sites.models import Site
from django.conf import settings
from django.core.urlresolvers import reverse
from django.forms.models import inlineformset_factory
from django.forms import formset_factory
from django.core.files import File
from django.utils.translation import ugettext_lazy as _

from certificates.models import Certificate
from certificates.forms import CertificateForm
from core.configs.models import AppConfig
from core.utils.mail.send_mail import send_mail,send_mail2
from core.utils.forms.utils import form_errors_to_json
from core.utils import get_file_name
from filer.utils.loader import load_model

CERTIFICATES_THANK_YOU = 'certificates_thank_you'

def certificate_order(request):
    from cms.models.pagemodel import Page
    from audio.models import Audio
    from video_files.models import VideoFile
    template = 'pages/certificate.html'
    ctx = {}
    if request.method == 'POST':
        form = CertificateForm(request.POST, request.FILES)
        if form.is_valid():
            attrs = form.cleaned_data.copy()
            attrs.pop("image")

            certificate = Certificate(**attrs)
            certificate_price = AppConfig.get_config('certificate_price', 0)
            certificate.price = float(certificate_price)

            if form.cleaned_data.get('image'):
                ImageModel = Certificate.photo.field.related_model
                file_name = get_file_name(form.cleaned_data.get('image').name)
                file_save_dir = os.path.join(settings.MEDIA_ROOT, 'certificates', 'photos')
                # Check if dir present
                try:
                    os.makedirs(file_save_dir)
                except OSError as e:
                    if not os.path.isdir(file_save_dir):
                        raise
                full_path = os.path.join(file_save_dir, file_name)
                if os.path.exists(full_path):
                    os.remove(full_path)
                try:
                    with open(full_path, 'wb') as f:
                        for chunk in form.cleaned_data.get('image').chunks():
                            f.write(chunk)
                        f.close()
                    img = File(open(full_path, 'r'))
                    _image, created = ImageModel.objects.get_or_create(file=img, defaults={
                        'name': file_name,
                        'description': 'Certificate Photo',
                    })
                    certificate.photo = _image
                except Exception:
                    raise
            certificate.save()
            context = {
                'site': Site.objects.get_current(),
                'object': certificate,
                'request': request,
            }
            email = AppConfig.get_config('certificate_form_email', settings.DEFAULT_FROM_EMAIL)
            send_mail(
                recipients=email,
                subject_template='certificates/mail/certificate_notify_subject.txt',
                html_template='certificates/mail/certificate_notify.html',
                text_template='certificates/mail/certificate_notify.txt',
                context=context,
            )
            if certificate.email:
                subject = AppConfig.get_config('certificate_mail_subject', _('Default mail subject'))
                send_mail2(
                    recipients=certificate.email,
                    subject=subject,
                    html_template='certificates/mail/certificate.html',
                    text_template='certificates/mail/certificate.txt',
                    context=context,
                )
            # detect redirect url
            cms_pages = Page.objects.filter(reverse_id='certificate_thank_you')
            if cms_pages:
                redirect_to = cms_pages[0].get_absolute_url()
            else:
                redirect_to = reverse('certificates:certificate_thank_you')
            if request.is_ajax():
                to_json = {
                    'success': True,
                    'location': redirect_to
                }
                return HttpResponse(json.dumps(to_json), content_type="application/json")
            else:
                return redirect(redirect_to)
        else:
            if request.is_ajax():
                return HttpResponse(form_errors_to_json(form), content_type="application/json")
    else:
        form = CertificateForm()

    ctx['form'] = form
    ctx['audio'] = None
    audios = Audio.objects.filter(content_type__isnull=True, object_id__isnull=True, enabled=True)
    if audios:
        ctx['audio'] = audios[0]

    ctx['vfile'] = None
    video_files = VideoFile.objects.filter(slug='certificates_video')
    if video_files:
        ctx['vfile'] = video_files[0]
    return render(request, template, ctx)


def certificate_thank_you(request):
    from my_placeholders.utils import check_placeholder
    template = 'thank_you.html'
    content_placeholder= check_placeholder(CERTIFICATES_THANK_YOU)
    ctx = {
        'content_placeholder': content_placeholder,
    }
    return render(request, template, ctx)
