# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


class CertificatesApp(CMSApp):
    app_name = 'certificates'
    name = _('Certificates')

    def get_urls(self, page=None, language=None, **kwargs):
        return ['certificates.urls']

apphook_pool.register(CertificatesApp)