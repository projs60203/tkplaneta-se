# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django.utils.translation import pgettext, ugettext_lazy as _
from django.forms.widgets import ClearableFileInput
from django.conf import settings
from django.utils.text import slugify

from dal import autocomplete

from core.configs.models import AppConfig
from certificates.models import Certificate


class CertificateForm(forms.ModelForm):
    image = forms.ImageField(
        label=_('Photo'),
        widget=ClearableFileInput(
            attrs={'class': '_image-field'}
        )
    )


    def __init__(self, *args, **kwargs):
        super(CertificateForm, self).__init__(*args, **kwargs)
        self.fields['name'].error_messages['required'] = _('Enter name, please')
        self.fields['region'].error_messages['required'] = _('Enter region, please')
        self.fields['city'].error_messages['required'] = _('Enter city, please')
        self.fields['phone'].required = False
        self.fields['phone'].error_messages['required'] = _('Enter Phone or email, please')
        self.fields['image'].error_messages['required'] = _('Enter photo, please')

    class Meta:
        model = Certificate
        fields = ('name', 'region', 'city', 'phone', 'email')
        widgets = {
            'name': forms.TextInput(attrs={'class': 'inputs', 'placeholder': _('Full Name')}),
            'region': forms.TextInput(attrs={'class': 'inputs', 'placeholder': _('Region')}),
            'city': forms.TextInput(attrs={'class': 'inputs', 'placeholder': _('City ')}),
            'phone': forms.TextInput(attrs={'class': 'inputs', 'placeholder': _('Phone')}),
            'email': forms.TextInput(attrs={'class': 'inputs', 'placeholder': _('Email')}),
            # 'photo': ClearableFileInput(
            #     attrs={'class': '_image-field'}
            # )
        }

    def clean(self):
        """
        Validate that the supplied email address or phone is present.
        """
        cleaned_data = super(CertificateForm, self).clean()
        email = cleaned_data.get('email')
        phone = cleaned_data.get('phone')
        if email or phone:
            pass
        else:
            error_list=self.errors.get('phone')
            if error_list is None:
                 error_list=forms.utils.ErrorList()
                 self.errors['phone']=error_list
            error_list.append(self.fields['phone'].error_messages["required"])
        return cleaned_data

