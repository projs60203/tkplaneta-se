# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from .signals import statistic_post_delete, statistic_post_save


class Statistic(models.Model):
    stat_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    stat_object = GenericForeignKey('stat_type', 'object_id')
    ip = models.GenericIPAddressField(_('IP-address'))
    timestamp = models.DateTimeField(_('date and time'), default=timezone.now)

    class Meta:
        verbose_name = _('statistic')
        verbose_name_plural = _('statistics')

models.signals.post_save.connect(
    statistic_post_save,
    sender=Statistic,
    dispatch_uid='statistics.Statistic.create'
)
models.signals.post_delete.connect(
    statistic_post_delete,
    sender=Statistic,
    dispatch_uid='statistics.Statistic.delete'
)