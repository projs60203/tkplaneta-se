# -*- coding: utf-8 -*-

def statistic_post_save(sender, instance, created, **kwargs):
    if not created:
        return
    update_comments_counters(instance)

def statistic_post_delete(sender, instance, **kwargs):
    update_comments_counters(instance)

def update_comments_counters(instance):
    from .models import Statistic
    stt_type = instance.stat_type
    obj_id = instance.object_id
    obj = stt_type.get_object_for_this_type(pk=obj_id)
    if hasattr(obj, 'clicks_count'):
        count = Statistic.objects.filter(stat_type=stt_type, object_id=obj_id).count()
        obj.clicks_count = count
        obj.save()
