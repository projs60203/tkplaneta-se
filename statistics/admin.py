# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from statistics.models import Statistic


admin.site.register(Statistic)
