# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url
from django.contrib import admin

from .views import background_manager, reload_plugin_page

admin.autodiscover()

urlpatterns = [
    url(r'^$', background_manager, name='background_manager'),
    url(r'^reload-plugin-page/(?P<plugin_pk>\d+)/$', reload_plugin_page, name='reload_plugin_page'),

]


