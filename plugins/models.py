# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _

from cms.models import CMSPlugin
from cms.models.fields import PageField
from orderedmodel.models import OrderedModel


from easy_thumbnails.files import get_thumbnailer
from filer.fields.image import FilerImageField
from djangocms_text_ckeditor.fields import HTMLField
from embed_video.fields import EmbedVideoField
from filer.utils.loader import load_model


class SiteBackgroundPluginModel(CMSPlugin):
    name = models.CharField(_('background name'), max_length=254, default='')

    image = FilerImageField(verbose_name=_("image"), related_name='background_image',
                            on_delete=models.DO_NOTHING)
    image2x = FilerImageField(verbose_name=_("image 2x"), related_name='background_image2x',
                              blank=True, null=True, on_delete=models.DO_NOTHING)

    url = models.CharField('URL', max_length=254)

    created_at = models.DateTimeField(verbose_name=_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('updated at'), auto_now=True)

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return '/go/?to=%d' % self.id

class ContentBackgroundPluginModel(CMSPlugin):
    name = models.CharField(_('background name'), max_length=254, default='')
    css_block_class = models.CharField(_('block css class'), max_length=254, default='')
    image = FilerImageField(verbose_name=_("image"), related_name='content_background_image',
                            on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(verbose_name=_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('updated at'), auto_now=True)

    def __unicode__(self):
        return self.name


class SiteLogoPluginModel(CMSPlugin):
    name = models.CharField(_('logo name'), max_length=254, default='')

    image = FilerImageField(verbose_name=_("image"), related_name='logo_image',
                            on_delete=models.DO_NOTHING)

    created_at = models.DateTimeField(verbose_name=_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('updated at'), auto_now=True)

    def __unicode__(self):
        return self.name


class VideoJSPlayerPluginModel(CMSPlugin):
    name = models.CharField(_('name'), max_length=254, default='')

    css_block_class = models.CharField(_('block css class'), max_length=255, blank=True, default='player_block mgb15')

    css_video_id = models.CharField(_('video css id'), max_length=255, blank=True, default='my_video_1')
    css_video_class = models.CharField(_('video css class'), max_length=255, blank=True, default='video-js vjs-default-skin')
    video_autoplay = models.BooleanField(_('video autoplay'), blank=True, default=True)
    video_preload = models.CharField(_('video preload'), max_length=255, blank=True, default='auto')
    video_width = models.PositiveIntegerField(_('video width'), blank=True, default=915)
    video_height = models.PositiveIntegerField(_('video height'), blank=True, default=514)

    is_multisource = models.BooleanField('multisource?', default=False, blank=True)

    created_at = models.DateTimeField(verbose_name=_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('updated at'), auto_now=True)

    def __unicode__(self):
        return self.name


class ViblastPlayerPluginModel(CMSPlugin):
    name = models.CharField(_('name'), max_length=254, default='')

    css_block_class = models.CharField(_('block css class'), max_length=255, blank=True, default='player_block mgb15')

    css_video_id = models.CharField(_('video css id'), max_length=255, blank=True, default='my_video_1')
    css_video_class = models.CharField(_('video css class'), max_length=255, blank=True, default='video-js vjs-default-skin')
    video_autoplay = models.BooleanField(_('video autoplay'), blank=True, default=True)
    video_preload = models.CharField(_('video preload'), max_length=255, blank=True, default='auto')
    video_width = models.PositiveIntegerField(_('video width'), blank=True, default=915)
    video_height = models.PositiveIntegerField(_('video height'), blank=True, default=514)
    data_vb_key = models.CharField(_('data viblest key'), max_length=255, blank=True, default='a086c7c6-ee10-4e94-8940-04e7e9ef42a5')

    created_at = models.DateTimeField(verbose_name=_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('updated at'), auto_now=True)

    def __unicode__(self):
        return self.name


class VideoSourcePluginModel(CMSPlugin):
    name = models.CharField(_('name'), max_length=254, default='')

    src_url = models.CharField(_('url'), max_length=254, default='')
    src_type = models.CharField(_('type'), max_length=254, default='')
    label = models.CharField(_('label'), max_length=8, default='', blank=True)

    created_at = models.DateTimeField(verbose_name=_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('updated at'), auto_now=True)

    def __unicode__(self):
        return self.name


class TextWEPluginModel(CMSPlugin):
    body = models.TextField(_('body'))

    def __unicode__(self):
        return self.body


class OwlCarouselPluginModel(CMSPlugin):
    name = models.CharField(_('carousel name'), max_length=254, default='')
    fixed = models.BooleanField(_('fixed'), default=False)

    created_at = models.DateTimeField(verbose_name=_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('updated at'), auto_now=True)

    def __unicode__(self):
        return self.name

    def copy_relations(self, oldinstance):
        super(OwlCarouselPluginModel, self).copy_relations(oldinstance)
        for picture in oldinstance.pictures.all().iterator():
            picture.pk = None
            picture.plugin = self
            picture.save()


class OwlCarouselItem(OrderedModel):
    plugin = models.ForeignKey(OwlCarouselPluginModel, related_name='pictures')
    size = models.CharField(_('size'), max_length=50, blank=True, default='344x866')
    image = FilerImageField(
        verbose_name=_('image'),
        related_name='carousel_images',
        on_delete=models.DO_NOTHING,
        help_text='344x866',
    )
    alt_tag = models.CharField(_('Alt tag'), max_length=255, blank=True)

    created_at = models.DateTimeField(verbose_name=_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('updated at'), auto_now=True)

    def __unicode__(self):
        return self.alt_tag


class ShortBlockPluginModel(CMSPlugin):
    css_class = models.CharField(_('css class'), max_length=255, blank=True, default='')

    caption = models.CharField(_('caption'), max_length=255, blank=True, default='')
    body = HTMLField(verbose_name=_('body'), blank=True, default='')

    created_at = models.DateTimeField(verbose_name=_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('updated at'), auto_now=True)

    def __unicode__(self):
        return self.caption


class ImageBlockPluginModel(CMSPlugin):
    css_class = models.CharField(_('css class'), max_length=255, blank=True, default='')
    caption = models.CharField(_('caption'), max_length=255, blank=True, default='')
    size = models.CharField(_('size'), max_length=50, blank=True, default='520x300')
    image = FilerImageField(
        verbose_name=_('image'),
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        help_text='520x300',
    )

    created_at = models.DateTimeField(verbose_name=_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('updated at'), auto_now=True)

    def __unicode__(self):
        return self.caption


class VideoBlockPluginModel(CMSPlugin):
    css_class = models.CharField(_('css class'), max_length=255, blank=True, default='')

    caption = models.CharField(_('caption'), max_length=255, blank=True, default='')

    image = FilerImageField(
        verbose_name=_('image'),
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )

    link = EmbedVideoField(_('link'), blank=True, default='')

    created_at = models.DateTimeField(verbose_name=_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('updated at'), auto_now=True)

    def __unicode__(self):
        return self.caption

    @property
    def upload_dir(self):
        return "video/video"

    def save(self, *args, **kwargs):
        from django.core.files import File
        from urlparse import urlparse
        import requests
        import os
        from django.conf import settings
        from embed_video.backends import detect_backend
        if self.image:
            pass
        else:
            if self.link:
                backend = detect_backend(self.link)
                if backend:
                    file_save_dir = os.path.join(settings.MEDIA_ROOT, self.upload_dir)
                    # Check if dir present
                    try:
                        os.makedirs(file_save_dir)
                    except OSError as e:
                        if not os.path.isdir(file_save_dir):
                            raise
                    r = requests.get(backend.get_thumbnail_url(), stream=True)
                    if r.status_code == 200:
                        thumbnail_url = r.request.path_url
                        filename = urlparse(thumbnail_url).path.split('/')[-1]
                        full_path = os.path.join(file_save_dir, filename)
                        if os.path.exists(full_path):
                            os.remove(full_path)
                        try:
                            with open(full_path, 'wb') as f:
                                for chunk in r:
                                    f.write(chunk)
                                f.close()
                            img = File(open(full_path, 'r'))
                            ImageModel = load_model(settings.FILER_IMAGE_MODEL)
                            _image, created = ImageModel.objects.get_or_create(file=img, defaults={
                                'name': filename,
                                'description': self.__unicode__(),
                            })
                            self.image = _image
                        except Exception as ee:
                            raise
        super(VideoBlockPluginModel, self).save(*args, **kwargs)


class PagedBlockPluginModel(CMSPlugin):
    css_class = models.CharField(_('css class'), max_length=255, blank=True, default='')

    caption = models.CharField(_('caption'), max_length=255, blank=True, default='')

    created_at = models.DateTimeField(verbose_name=_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('updated at'), auto_now=True)

    def __unicode__(self):
        return self.caption

    def copy_relations(self, oldinstance):
        super(PagedBlockPluginModel, self).copy_relations(oldinstance)
        for page in oldinstance.pages.iterator():
            page.pk = None
            page.plugin = self
            page.save()


class BlockTextPage(OrderedModel):
    plugin = models.ForeignKey(PagedBlockPluginModel, related_name='pages')

    name = models.CharField(_('name'), max_length=255, blank=True, default='')

    body = HTMLField(verbose_name=_('body'), blank=True, default='')

    created_at = models.DateTimeField(verbose_name=_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('updated at'), auto_now=True)

    def __unicode__(self):
        return self.name
