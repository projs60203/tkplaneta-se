# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import Http404
from django.shortcuts import redirect, get_object_or_404, render
from django.contrib.contenttypes.models import ContentType

from ipware.ip import get_ip

from statistics.models import Statistic
from core.utils.pagination import paginator_factory


from .models import SiteBackgroundPluginModel, PagedBlockPluginModel


def background_manager(request):
    to = request.GET.get('to', None)
    if to is None or not to.isdigit():
        raise Http404
    clicked_banner = get_object_or_404(SiteBackgroundPluginModel, id=to)
    banner_type = ContentType.objects.get_for_model(clicked_banner)
    stats_count = Statistic.objects.filter(stat_type=banner_type, object_id=clicked_banner.pk, ip=get_ip(request)).count()
    if stats_count == 0:
        Statistic.objects.create(stat_object=clicked_banner, ip=get_ip(request))
    return redirect(clicked_banner.url)


def reload_plugin_page(request, plugin_pk):
    plugin_page = request.GET.get('plugin_page', '1')
    instance = get_object_or_404(PagedBlockPluginModel, pk=plugin_pk)
    objects_list = instance.pages.all()
    objects_list = paginator_factory(request, objects_list, num_pages=1, page='plugin_page')
    return render(request, 'plugins/paged_block_body.html', {
        'request': request,
        'instance': instance,
        'objects_list': objects_list,
        'plugin_page': plugin_page,
    })
