from django import forms
from django.contrib import admin
from django.db import models
from django.utils.translation import ugettext as _

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from core.utils.pagination import paginator_factory

from .models import (
    SiteBackgroundPluginModel, ContentBackgroundPluginModel,
    SiteLogoPluginModel, TextWEPluginModel,
    OwlCarouselItem, OwlCarouselPluginModel,
    ShortBlockPluginModel, ImageBlockPluginModel, VideoBlockPluginModel,
    PagedBlockPluginModel, BlockTextPage,
    VideoJSPlayerPluginModel, ViblastPlayerPluginModel, VideoSourcePluginModel,
)


class SiteBackgroundPlugin(CMSPluginBase):
    model = SiteBackgroundPluginModel
    module = _("Image link items")
    name = _("Site Background")
    render_template = 'plugins/site_background.html'
    allow_children = False

    def render(self, context, instance, placeholder):
        context = super(SiteBackgroundPlugin, self).render(context, instance, placeholder)
        return context
plugin_pool.register_plugin(SiteBackgroundPlugin)


class ContentBackgroundPlugin(CMSPluginBase):
    model = ContentBackgroundPluginModel
    module = _("Content")
    name = _("Content background")
    render_template = 'plugins/content_background.html'
    allow_children = True

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context
plugin_pool.register_plugin(ContentBackgroundPlugin)


class SiteLogoPlugin(CMSPluginBase):
    model = SiteLogoPluginModel
    module = _("Image link items")
    name = _("Site Logo")
    render_template = 'plugins/site_logo.html'
    allow_children = False

    def render(self, context, instance, placeholder):
        context = super(SiteLogoPlugin, self).render(context, instance, placeholder)
        return context
plugin_pool.register_plugin(SiteLogoPlugin)


class VideoJSPlayerPlugin(CMSPluginBase):
    model = VideoJSPlayerPluginModel
    module = _("Content")
    name = _("Video JS Player")
    render_template = 'plugins/video_js_player.html'
    allow_children = True
    child_classes = ['VideoJSSourcePlugin']

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context
plugin_pool.register_plugin(VideoJSPlayerPlugin)


class ViblastPlayerPlugin(CMSPluginBase):
    model = ViblastPlayerPluginModel
    module = _("Content")
    name = _("Viblast Player")
    render_template = 'plugins/viblast_player.html'
    allow_children = True
    child_classes = ['VideoJSSourcePlugin']

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context
plugin_pool.register_plugin(ViblastPlayerPlugin)


class VideoJSSourcePlugin(CMSPluginBase):
    model = VideoSourcePluginModel
    module = _("Content")
    name = _("Video JS Source")
    render_template = "plugins/video_source_plugin.html"
    require_parent = True
    parent_classes = ['VideoJSPlayerPlugin']
    disable_child_plugins = True

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context
plugin_pool.register_plugin(VideoJSSourcePlugin)


class TextWEPlugin(CMSPluginBase):
    model = TextWEPluginModel
    module = _('Generic')
    name = _('Text without editor')
    disable_child_plugins = True
    render_template = "cms/plugins/text.html"

    def render(self, context, instance, placeholder):
        context.update({'body': instance})
        return context
plugin_pool.register_plugin(TextWEPlugin)


class OwlCarouselItemInline(admin.TabularInline):
    model = OwlCarouselItem
    extra = 1

    def formfield_for_dbfield(self, db_field, **kwargs):
        if isinstance(db_field, models.TextField):
            modified_text_field = db_field.formfield()
            modified_text_field.widget = forms.Textarea(attrs={'cols': 30, 'rows': 3})
            return modified_text_field
        return super(OwlCarouselItemInline, self).formfield_for_dbfield(db_field, **kwargs)


class OwlCarouselPlugin(CMSPluginBase):
    model = OwlCarouselPluginModel
    module = _("Content")
    name = _("OwlCarousel")
    render_template = 'plugins/owl_carousel.html'
    # allow_children = True
    # child_classes = ['OwlCarouselItemPlugin', ]

    inlines = [OwlCarouselItemInline]

    def render(self, context, instance, placeholder):
        context.update({
            'instance': instance,
            'objects_list': instance.pictures.select_related('image'),
        })
        return context
plugin_pool.register_plugin(OwlCarouselPlugin)


class ShortBlockPlugin(CMSPluginBase):
    model = ShortBlockPluginModel
    module = _("Content")
    name = _("ShortBlock")
    render_template = 'plugins/short_block.html'

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context
plugin_pool.register_plugin(ShortBlockPlugin)


class ImageShortBlockPlugin(CMSPluginBase):
    model = ImageBlockPluginModel
    module = _("Content")
    name = _("ImageBlock")
    render_template = 'plugins/image_short_block.html'

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context
plugin_pool.register_plugin(ImageShortBlockPlugin)


class VideoBlockPlugin(CMSPluginBase):
    model = VideoBlockPluginModel
    module = _("Content")
    name = _("VideoBlock")
    render_template = 'plugins/video_block.html'

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context
plugin_pool.register_plugin(VideoBlockPlugin)


class BlockTextPageInline(admin.StackedInline):
    model = BlockTextPage
    extra = 1

    # def formfield_for_dbfield(self, db_field, **kwargs):
    #     if isinstance(db_field, models.TextField):
    #         modified_text_field = db_field.formfield()
    #         modified_text_field.widget = forms.Textarea(attrs={'cols': 30, 'rows': 3})
    #         return modified_text_field
    #     return super(BlockTextPageInline, self).formfield_for_dbfield(db_field, **kwargs)


class PaginatedBlockPlugin(CMSPluginBase):
    model = PagedBlockPluginModel
    module = _("Content")
    name = _("Block with pages")
    render_template = 'plugins/paged_block.html'

    inlines = [BlockTextPageInline]

    def render(self, context, instance, placeholder):
        objects_list = instance.pages.all()
        request = context['request']
        objects_list = paginator_factory(request, objects_list, num_pages=1, page='plugin_page')
        context.update({
            'instance': instance,
            'objects_list': objects_list,
        })
        return context
plugin_pool.register_plugin(PaginatedBlockPlugin)
