# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class Contacts2Config(AppConfig):
    name = 'contacts_2'
    verbose_name = _('Contacts 2')
