# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


class Contacts2App(CMSApp):
    app_name = 'contacts_2'
    name = _('Contacts 2')

    def get_urls(self, page=None, language=None, **kwargs):
        return ['contacts_2.urls']

apphook_pool.register(Contacts2App)