# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.sites.models import Site
from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _


from core.configs.models import AppConfig
from core.utils.mail.send_mail import send_mail,send_mail2
from core.utils.forms.utils import form_errors_to_json
from contacts_2.forms import ContactForm


CONTACTS_2_THANK_YOU = 'contacts_2_thank_you'

def index(request):
    from cms.models.pagemodel import Page
    template = 'pages/contacts.html'
    ctx = {}
    if request.method == 'POST':
        form = ContactForm(request.POST, request.FILES)

        if form.is_valid():
            data = form.cleaned_data
            context = {
                'site': Site.objects.get_current(),
                'data': data,
            }
            email = AppConfig.get_config('contact_form_email', settings.DEFAULT_FROM_EMAIL)
            send_mail(
                recipients=email,
                subject_template='contacts_2/mail/contacts_notify_subject.txt',
                html_template='contacts_2/mail/contacts_notify.html',
                text_template='contacts_2/mail/contacts_notify.txt',
                context=context,
            )
            sender_email = data.get('email')
            if sender_email:
                subject = AppConfig.get_config('contact_mail_subject', _('Default mail subject'))
                send_mail2(
                    recipients=sender_email,
                    subject=subject,
                    html_template='contacts_2/mail/contacts.html',
                    text_template='contacts_2/mail/contacts.txt',
                    context=context,
                )

            # detect redirect url
            cms_pages = Page.objects.filter(reverse_id=CONTACTS_2_THANK_YOU)
            if cms_pages:
                redirect_to = cms_pages[0].get_absolute_url()
            else:
                redirect_to = reverse('contacts_2:thank_you')
            if request.is_ajax():
                to_json = {
                    'success': True,
                    'location': redirect_to
                }
                return HttpResponse(json.dumps(to_json), content_type="application/json")
            else:
                return redirect(redirect_to)
        elif request.is_ajax():
           return HttpResponse(form_errors_to_json(form), content_type="application/json")
    else:
        form = ContactForm()
    ctx['form'] = form
    return render(request, template, ctx)


def thank_you(request):
    from my_placeholders.utils import check_placeholder
    content_placeholder= check_placeholder(CONTACTS_2_THANK_YOU)
    template = 'thank_you.html'
    ctx = {
        'content_placeholder': content_placeholder,
    }
    return render(request, template, ctx)
