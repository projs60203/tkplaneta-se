# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url
from django.contrib import admin

from contacts_2.views import index, thank_you

admin.autodiscover()

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^thank-you/$', thank_you, name='thank_you'),
]
