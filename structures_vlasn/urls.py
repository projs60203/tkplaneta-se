# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url
from django.contrib import admin

from structures_vlasn.views import index

admin.autodiscover()

urlpatterns = [
    url(r'^$', index, name='index'),
    # url(r'^thank-you$', thank_you, name='thank_you'),
]
