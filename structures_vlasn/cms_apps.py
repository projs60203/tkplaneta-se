from __future__ import unicode_literals

from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


class StructuresVlasnApp(CMSApp):
    app_name = 'structures_vlasn'
    name = _('Structures Vlasnosti')

    def get_urls(self, page=None, language=None, **kwargs):
        return ['structures_vlasn.urls']

apphook_pool.register(StructuresVlasnApp)