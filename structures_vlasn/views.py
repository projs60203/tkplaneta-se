# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from core.utils.pagination import paginator_factory
from django.shortcuts import render
from structures_vlasn.models import StructureVlasn

def index(request):
    template = 'pages/structures.html'
    structures = StructureVlasn.objects.filter(enabled=True).order_by('-year')
    structures = paginator_factory(request, structures, 1)
    ctx = {
        'structures': structures,
    }
    return render(request, template, ctx)
