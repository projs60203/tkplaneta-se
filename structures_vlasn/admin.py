# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models import StructureVlasn

# Register your models here.
class StructureAdmin(admin.ModelAdmin):
    # list_display = ('last_name', 'first_name', 'patronymic', 'enabled', 'email', 'phone')
    # search_fields = ('first_name', 'last_name', 'patronymic', 'email', 'phone')
    # list_display_links = ('last_name', 'first_name', 'patronymic')
    list_display = ('admin_thumbnail', 'name', 'year', 'enabled')
    list_display_links = ('admin_thumbnail', 'name')
    readonly_fields = ('created_at', 'updated_at')
    list_editable = ('enabled',)

admin.site.register(StructureVlasn, StructureAdmin)
