# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from filer.fields.image import FilerImageField
from easy_thumbnails.files import get_thumbnailer


@python_2_unicode_compatible
class StructureVlasn(models.Model):
    enabled = models.BooleanField(_('enabled?'), default=True, blank=True)

    name = models.CharField(_('name'), max_length=254)
    year = models.PositiveIntegerField(_('year'))

    image = FilerImageField(
        verbose_name=_('image'),
        help_text='3512x2124',
        on_delete=models.DO_NOTHING,
    )

    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('updated at'), auto_now=True)

    def __str__(self):
        return self.name

    def admin_thumbnail(self):
        if self.image:
            thumb_url = get_thumbnailer(self.image).get_thumbnail({'size': (70, 42), 'quality': 100}).url
            return '<img src="{}" />'.format(thumb_url)
        else:
            return ''
    admin_thumbnail.short_description = _('image')
    admin_thumbnail.allow_tags = True

    class Meta:
        ordering = ['-year']
        verbose_name = _('structure')
        verbose_name_plural = _('structures')
