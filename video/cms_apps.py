# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from aldryn_apphooks_config.app_base import CMSConfigApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _

from video.cms_appconfig import VideoConfig

class VideoApp(CMSConfigApp):
    name = _('Video')
    app_name = 'video'
    app_config = VideoConfig

    def get_urls(self, *args, **kwargs):
        return ['video.urls']

    # NOTE: Please do not add a «menu» here, menu’s should only be added by at
    # the discretion of the operator.


apphook_pool.register(VideoApp)
