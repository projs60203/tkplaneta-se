# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes import fields as generic
from django.contrib.contenttypes.models import ContentType

# from djangocms_text_ckeditor.fields import HTMLField
from ckeditor.fields import RichTextField
from embed_video.fields import EmbedVideoField
from filer.fields.image import FilerImageField
from easy_thumbnails.files import get_thumbnailer
from filer.utils.loader import load_model
from parler.models import TranslatableModel, TranslatedFields
from aldryn_apphooks_config.fields import AppHookConfigField
from aldryn_translation_tools.models import TranslationHelperMixin


from .cms_appconfig import VideoConfig
from .managers import VideoManager

# _play_icon = root_path('video/static/video/img/play_icon.png')
# _tv_icon = root_path('video/static/video/img/tv.png')


@python_2_unicode_compatible
class Video(TranslationHelperMixin, TranslatableModel):
    content_type = models.ForeignKey(ContentType, null=True)
    object_id = models.PositiveIntegerField(null=True)
    content_object = generic.GenericForeignKey()
    display_order = models.PositiveIntegerField(_('display order'), default=0)
    enabled = models.BooleanField(_('enabled'), default=True)

    translations = TranslatedFields(
        name = models.CharField(_('name'), max_length=255, blank=True, default=''),
        description = RichTextField(verbose_name=_('description'), blank=True, default='')
    )

    image = FilerImageField(
        verbose_name=_('image'),
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )

    link = EmbedVideoField(_('link'), blank=True, default='')

    app_config = AppHookConfigField(
        VideoConfig,
        verbose_name=_('Section'),
        help_text='',
    )

    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('updated at'), auto_now=True)

    objects = VideoManager()

    class Meta:
        ordering = ['display_order']
        verbose_name = _('video')
        verbose_name_plural = _('video')

    def admin_thumbnail(self):
        if self.image:
            thumb_url = get_thumbnailer(self.image).get_thumbnail({'size': (155, 39), 'quality': 100}).url
            return '<img src="{}" />'.format(thumb_url)
        else:
            return ''
    admin_thumbnail.short_description = _('image')
    admin_thumbnail.allow_tags = True

    @property
    def upload_dir(self):
        return "video/video"

    def save(self, *args, **kwargs):
        from django.core.files import File
        from urlparse import urlparse
        import requests
        import os
        from django.conf import settings
        from embed_video.backends import detect_backend
        if self.image:
            pass
        else:
            if self.link:
                backend = detect_backend(self.link)
                if backend:
                    file_save_dir = os.path.join(settings.MEDIA_ROOT, self.upload_dir)
                    # Check if dir present
                    try:
                        os.makedirs(file_save_dir)
                    except OSError as e:
                        if not os.path.isdir(file_save_dir):
                            raise
                    r = requests.get(backend.get_thumbnail_url(), stream=True)
                    if r.status_code == 200:
                        thumbnail_url = r.request.path_url
                        filename = urlparse(thumbnail_url).path.split('/')[-1]
                        full_path = os.path.join(file_save_dir, filename)
                        if os.path.exists(full_path):
                            os.remove(full_path)
                        try:
                            with open(full_path, 'wb') as f:
                                for chunk in r:
                                    f.write(chunk)
                                f.close()
                            img = File(open(full_path, 'r'))
                            ImageModel = load_model(settings.FILER_IMAGE_MODEL)
                            _image, created = ImageModel.objects.get_or_create(file=img, defaults={
                                'name': 'image',
                                'description': self.__str__(),
                            })
                            self.image = _image
                        except Exception as ee:
                            raise
        super(Video, self).save(*args, **kwargs)

    def __str__(self):
        return self.safe_translation_getter('name')

