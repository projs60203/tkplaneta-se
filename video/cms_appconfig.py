# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django import forms
from django.conf import settings
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from parler.models import TranslatableModel, TranslatedFields
from aldryn_apphooks_config.models import AppHookConfig
from aldryn_apphooks_config.utils import setup_config

from app_data import AppDataForm
from cms.models.fields import PlaceholderField


class VideoConfig(TranslatableModel, AppHookConfig):
    translations = TranslatedFields(
        app_title=models.CharField(_('name'), max_length=234),
    )
    count_in_row = models.PositiveIntegerField(
        _('Count in row'),
        blank=False,
        default=3,
        help_text=_('Count video in Row'),
    )
    row_count_in_page = models.PositiveIntegerField(
        _('Row count in page'),
        blank=False,
        default=3,
        help_text=_('Row Count in Page'),
    )
    pagination_pages_start = models.PositiveIntegerField(
        _('Pagination pages start'),
        blank=False,
        default=10,
        help_text=_('When paginating list views, after how many pages '
                    'should we start grouping the page numbers.'),
    )
    pagination_pages_visible = models.PositiveIntegerField(
        _('Pagination pages visible'),
        blank=False,
        default=3,
        help_text=_('When grouping page numbers, this determines how many '
                    'pages are visible on each side of the active page.'),
    )
    header = PlaceholderField('video_header',
                               related_name='video_header')

    @property
    def paginate_by(self):
        return self.count_in_row * self.row_count_in_page

    def get_app_title(self):
        return getattr(self, 'app_title', _('untitled'))

    class Meta:
        verbose_name = _('Section')
        verbose_name_plural = _('Sections')

    # def __str__(self):
    #     result = super(AppHookConfig)
    #     return result


class VideoConfigForm(AppDataForm):
    pass

setup_config(VideoConfigForm, VideoConfig)
