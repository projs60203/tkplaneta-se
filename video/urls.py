# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url
from django.contrib import admin

from video.views import VideoList

admin.autodiscover()
urlpatterns = [
    url(r'^$', VideoList.as_view(), name='video-lists'),
]
