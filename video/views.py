# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db.models import Q
from django.http import (
    Http404,
    HttpResponseRedirect,
    HttpResponsePermanentRedirect,
)
from django.shortcuts import get_object_or_404
from django.utils import translation
from django.views.generic import ListView
from django.utils import translation

from aldryn_apphooks_config.mixins import AppConfigMixin
from video.utils.utilities import get_valid_languages_from_request
from video.compat import toolbar_edit_mode_active
from parler.views import TranslatableSlugMixin, ViewUrlMixin

from .models import Video, VideoConfig


class EditModeMixin(object):
    """
    A mixin which sets the property 'edit_mode' with the truth value for
    whether a user is logged-into the CMS and is in edit-mode.
    """
    edit_mode = False

    def dispatch(self, request, *args, **kwargs):
        self.edit_mode = (
            self.request.toolbar and toolbar_edit_mode_active(self.request))
        return super(EditModeMixin, self).dispatch(request, *args, **kwargs)


class PreviewModeMixin(EditModeMixin):
    """
    If content editor is logged-in, show all articles. Otherwise, only the
    published videos should be returned.
    """
    def get_queryset(self):
        qs = super(PreviewModeMixin, self).get_queryset()
        # check if user can see unpublished items. this will allow to switch
        # to edit mode instead of 404 on article detail page. CMS handles the
        # permissions.

        # user = self.request.user
        # user_can_edit = user.is_staff or user.is_superuser
        # if not (self.edit_mode or user_can_edit):
        #     qs = qs.published()
        language = translation.get_language()
        qs = qs.active_translations(language).namespace(self.namespace)
        return qs


class AppHookCheckMixin(object):

    def dispatch(self, request, *args, **kwargs):
        self.valid_languages = get_valid_languages_from_request(
            self.namespace, request)
        return super(AppHookCheckMixin, self).dispatch(
            request, *args, **kwargs)

    def get_queryset(self):
        # filter available objects to contain only resolvable for current
        # language. IMPORTANT: after .translated - we cannot use .filter
        # on translated fields (parler/django limitation).
        # if your mixin contains filtering after super call - please place it
        # after this mixin.
        qs = super(AppHookCheckMixin, self).get_queryset()
        return qs.translated(*self.valid_languages)


class VideoListBase(AppConfigMixin, AppHookCheckMixin,
                      PreviewModeMixin, ListView):
    model = Video
    show_header = False

    def get_paginate_by(self, queryset):
        if self.paginate_by is not None:
            return self.paginate_by
        else:
            try:
                return self.config.paginate_by
            except AttributeError:
                return 9  # sensible failsafe

    def get_pagination_options(self):
        # Django does not handle negative numbers well
        # when using variables.
        # So we perform the conversion here.
        if self.config:
            options = {
                'pages_start': self.config.pagination_pages_start,
                'pages_visible': self.config.pagination_pages_visible,
            }
        else:
            options = {
                'pages_start': 9,
                'pages_visible': 3,
            }

        pages_visible_negative = -options['pages_visible']
        options['pages_visible_negative'] = pages_visible_negative
        options['pages_visible_total'] = options['pages_visible'] + 1
        options['pages_visible_total_negative'] = pages_visible_negative - 1
        return options

    def get_context_data(self, **kwargs):
        context = super(VideoListBase, self).get_context_data(**kwargs)
        # video_list = Video.objects.filter()
        context['pagination'] = self.get_pagination_options()
        context['config'] = self.config
        return context


class VideoList(VideoListBase):
    """A complete list of articles."""
    template_name = 'pages/video.html'
    show_header = True

    def get_queryset(self):
        qs = super(VideoList, self).get_queryset()
        qs = qs.filter(content_type__isnull=True, object_id__isnull=True).filter(enabled=True)
        return qs

    def get_context_data(self, **kwargs):
        from core.utils.helpers import split_as_rows
        context = super(VideoList, self).get_context_data(**kwargs)
        count_in_row = self.get_row_length()
        video_list = context['video_list']
        context['video_rows'] = split_as_rows(video_list, count_in_row)
        return context

    def get_row_length(self):
        try:
            return self.config.count_in_row
        except AttributeError:
            return 3  # sensible failsafe
