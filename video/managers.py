# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from aldryn_apphooks_config.managers.base import ManagerMixin, QuerySetMixin
from parler.managers import TranslatableManager, TranslatableQuerySet

class VideoQuerySet(QuerySetMixin, TranslatableQuerySet):
    pass

class VideoManager(ManagerMixin, TranslatableManager):

    def get_queryset(self):
        qs = VideoQuerySet(self.model, using=self.db)
        return qs.select_related('image')

