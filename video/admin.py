# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from parler.admin import TranslatableAdmin
from parler.forms import TranslatableModelForm
from cms.admin.placeholderadmin import (
    FrontendEditableAdminMixin,
    PlaceholderAdminMixin
)
from aldryn_translation_tools.admin import AllTranslationsMixin

from aldryn_apphooks_config.admin import BaseAppHookConfig, ModelAppHookConfig

from video import models

class VideoAdminForm(TranslatableModelForm):

    class Meta:
        model = models.Video
        fields = [
            'app_config',
            'display_order',
            'enabled',
            'link',
            'name',
            'description',
            'image',
        ]

    def __init__(self, *args, **kwargs):
        super(VideoAdminForm, self).__init__(*args, **kwargs)
        #
        # qs = models.Video.objects
        #
        # if self.instance.pk:
        #     qs = qs.exclude(pk=self.instance.pk)

class VideoAdmin(
    AllTranslationsMixin,
    ModelAppHookConfig,
    TranslatableAdmin
):
    form = VideoAdminForm
    list_display = ('admin_thumbnail', 'name', 'app_config', 'display_order', 'enabled', )
    list_display_links = ('admin_thumbnail', 'name')

    readonly_fields = ('created_at', 'updated_at', )
    list_editable = ('display_order', 'enabled')
    list_filter    = ('app_config', 'enabled', 'created_at', 'updated_at',)
    search_fields  = ('translations__name', 'translations__description',)

    def queryset(self, request):
        qs = super(VideoAdmin, self).queryset(request)
        return qs.filter(content_type__isnull=True, object_id__isnull=True)


admin.site.register(models.Video, VideoAdmin)


class VideoConfigAdmin(
    AllTranslationsMixin,
    BaseAppHookConfig,
    PlaceholderAdminMixin,
    TranslatableAdmin
):
    def get_config_fields(self):
        return (
            'app_title',
            'count_in_row', 'row_count_in_page',
            'pagination_pages_start',
            'pagination_pages_visible',
        )


admin.site.register(models.VideoConfig, VideoConfigAdmin)
