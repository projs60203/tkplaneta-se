# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.contrib.gis.db import models


class AppConfigs(models.Model):
    key = models.CharField(max_length=255)
    value = models.TextField()
    value_ru = models.TextField(blank=True, null=True)
    value_uk = models.TextField(blank=True, null=True)
    is_disable = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'app_configs'


class AudioAudio(models.Model):
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    object_id = models.IntegerField(blank=True, null=True)
    position = models.IntegerField()
    enabled = models.BooleanField()
    name = models.CharField(max_length=255)
    description = models.TextField()
    track_aac = models.CharField(max_length=100)
    track_ogg = models.CharField(max_length=100)
    track_wav = models.CharField(max_length=100)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'audio_audio'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=50)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField()
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=30)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=75)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class BannerBanner(models.Model):
    title = models.CharField(max_length=100)
    image = models.CharField(max_length=100)
    url = models.CharField(max_length=200)
    enabled = models.BooleanField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    clicks_count = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'banner_banner'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    name = models.CharField(max_length=100)
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class DjangoSite(models.Model):
    domain = models.CharField(max_length=100)
    name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'django_site'


class MenuItem(models.Model):
    position = models.IntegerField()
    url = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    name_ru = models.CharField(max_length=255, blank=True, null=True)
    name_uk = models.CharField(max_length=255, blank=True, null=True)
    location = models.CharField(max_length=255)
    class_field = models.CharField(max_length=255, blank=True, null=True)
    search_template = models.CharField(max_length=255)
    created_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'menu_item'


class MenuSubitem(models.Model):
    item = models.ForeignKey(MenuItem, models.DO_NOTHING, blank=True, null=True)
    url = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    position = models.IntegerField()
    created_at = models.DateTimeField()
    name_ru = models.CharField(max_length=255, blank=True, null=True)
    name_uk = models.CharField(max_length=255, blank=True, null=True)
    search_template = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'menu_subitem'


class MetaTags(models.Model):
    url = models.CharField(max_length=100)
    content_type = models.ForeignKey(DjangoContentType, models.DO_NOTHING, blank=True, null=True)
    object_id = models.IntegerField(blank=True, null=True)
    title = models.CharField(max_length=80)
    title_ru = models.CharField(max_length=80, blank=True, null=True)
    title_uk = models.CharField(max_length=80, blank=True, null=True)
    keywords = models.CharField(max_length=250)
    keywords_ru = models.CharField(max_length=250, blank=True, null=True)
    keywords_uk = models.CharField(max_length=250, blank=True, null=True)
    description = models.TextField()
    description_ru = models.TextField(blank=True, null=True)
    description_uk = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'meta_tags'
        unique_together = (('content_type', 'object_id'),)


class MusicsMusic(models.Model):
    name = models.CharField(max_length=255)
    name_ru = models.CharField(max_length=255, blank=True, null=True)
    name_uk = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'musics_music'


class NewVideoNewvideo(models.Model):
    content_type = models.ForeignKey(DjangoContentType, models.DO_NOTHING, blank=True, null=True)
    object_id = models.IntegerField(blank=True, null=True)
    position = models.IntegerField()
    name = models.CharField(max_length=255)
    name_ru = models.CharField(max_length=255, blank=True, null=True)
    name_uk = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField()
    description_ru = models.TextField(blank=True, null=True)
    description_uk = models.TextField(blank=True, null=True)
    image = models.CharField(max_length=100)
    link = models.CharField(max_length=200)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'new_video_newvideo'


class OrdersAnnouncement(models.Model):
    name = models.CharField(max_length=255)
    phone = models.CharField(max_length=255)
    email = models.CharField(max_length=75)
    text = models.TextField()
    status = models.CharField(max_length=255)
    created_at = models.DateTimeField()
    payment_type = models.CharField(max_length=255)
    price = models.FloatField()
    uuid_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'orders_announcement'


class OrdersCertificate(models.Model):
    name = models.CharField(max_length=255)
    region = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    phone = models.CharField(max_length=255)
    email = models.CharField(max_length=75)
    photo = models.CharField(max_length=100)
    status = models.CharField(max_length=255)
    created_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'orders_certificate'


class OrdersCongratulation(models.Model):
    sender_name = models.CharField(max_length=255)
    phone = models.CharField(max_length=255)
    email = models.CharField(max_length=75)
    recipient_name = models.CharField(max_length=255)
    congratulator = models.CharField(max_length=255)
    wish = models.TextField()
    music_customer = models.CharField(max_length=255)
    status = models.CharField(max_length=255)
    created_at = models.DateTimeField()
    payment_type = models.CharField(max_length=255)
    price = models.FloatField()
    uuid_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'orders_congratulation'


class PageSectionsSection(models.Model):
    page = models.ForeignKey('PageSectionsSectionpage', models.DO_NOTHING)
    title = models.TextField()
    title_ru = models.TextField(blank=True, null=True)
    title_uk = models.TextField(blank=True, null=True)
    slug = models.CharField(unique=True, max_length=255)
    content = models.TextField()
    content_ru = models.TextField(blank=True, null=True)
    content_uk = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'page_sections_section'


class PageSectionsSectionpage(models.Model):
    url = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    slug = models.CharField(unique=True, max_length=255)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'page_sections_sectionpage'


class PageSectionsSubpage(models.Model):
    section = models.ForeignKey(PageSectionsSection, models.DO_NOTHING)
    position = models.IntegerField()
    content = models.TextField()
    content_ru = models.TextField(blank=True, null=True)
    content_uk = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'page_sections_subpage'


class PagesPage(models.Model):
    url = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    title_ru = models.CharField(max_length=255, blank=True, null=True)
    title_uk = models.CharField(max_length=255, blank=True, null=True)
    show_title = models.BooleanField()
    content = models.TextField()
    content_ru = models.TextField(blank=True, null=True)
    content_uk = models.TextField(blank=True, null=True)
    template = models.CharField(max_length=255)
    created_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'pages_page'


class PagesPagegallery(models.Model):
    page = models.ForeignKey(PagesPage, models.DO_NOTHING, blank=True, null=True)
    title = models.CharField(max_length=255)
    title_ru = models.CharField(max_length=255, blank=True, null=True)
    title_uk = models.CharField(max_length=255, blank=True, null=True)
    image = models.CharField(max_length=100)
    video = models.CharField(max_length=255)
    description = models.TextField()
    description_ru = models.TextField(blank=True, null=True)
    description_uk = models.TextField(blank=True, null=True)
    position = models.IntegerField()
    created_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'pages_pagegallery'


class SouthMigrationhistory(models.Model):
    app_name = models.CharField(max_length=255)
    migration = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'south_migrationhistory'


class StatisticsStatistic(models.Model):
    stat_type = models.ForeignKey(DjangoContentType, models.DO_NOTHING)
    object_id = models.IntegerField()
    ip = models.GenericIPAddressField()
    timestamp = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'statistics_statistic'


def get_file_path(obj, filename):
    import os
    import uuid
    if hasattr(obj, 'upload_dir'):
        extension = filename.split('.')[-1]
        filename = "%s.%s" % (uuid.uuid4(), extension)
        return os.path.join(obj.upload_dir, filename)
    else:
        raise AttributeError("%s does not have 'upload_dir' attribute" % obj.__class__.__name__)


class TestimonialsTestimonial(models.Model):
    enabled = models.BooleanField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    name = models.CharField(max_length=30)
    image = models.ImageField(
        verbose_name='image',
        upload_to=get_file_path, blank=True, default='',
    )

    class Meta:
        ordering = ['-created_at']
        managed = False
        db_table = 'testimonials_testimonial'

    @property
    def upload_dir(self):
        return "testimonials/pages"



class VideoVideo(models.Model):
    content_type = models.ForeignKey(DjangoContentType, models.DO_NOTHING, blank=True, null=True)
    object_id = models.IntegerField(blank=True, null=True)
    position = models.IntegerField()
    name = models.CharField(max_length=255)
    name_ru = models.CharField(max_length=255, blank=True, null=True)
    name_uk = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField()
    description_ru = models.TextField(blank=True, null=True)
    description_uk = models.TextField(blank=True, null=True)
    image = models.CharField(max_length=100)
    link = models.CharField(max_length=200)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'video_video'
