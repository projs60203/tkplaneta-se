# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function, unicode_literals

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.contrib.sitemaps import GenericSitemap
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.static import serve
from django.views.generic.base import RedirectView
from django.views.generic import TemplateView
from django.utils.timezone import now

from cms.sitemaps import CMSSitemap

from artists.views import ArtistAutocomplete, CompositionAutocomplete

admin.autodiscover()

urlpatterns = [
    url(r'^favicon\.ico$', RedirectView.as_view(url='/static/img/favicon.ico')),
    url(r'^pie\.htc$', RedirectView.as_view(url='/static/css/pie.htc')),
    url(r'^sitemap\.xml$', sitemap,
        {'sitemaps': {'cmspages': CMSSitemap}}),
    # dal views
    url(r'^artist-autocomplete/$', ArtistAutocomplete.as_view(), name='artist-autocomplete'),
    url(r'^composition-autocomplete/$', CompositionAutocomplete.as_view(), name='composition-autocomplete'),
    # end dal views
    url('^go/', include('plugins.urls')),
]

urlpatterns += i18n_patterns(
    url(r'^filebrowser_filer/', include('ckeditor_filebrowser_filer.urls')),
    url(r'^admin/', include(admin.site.urls)),  # NOQA
    url(r'^doska/', include('doska.urls', namespace='doska')),
    # url(r'^cengratulations/', include('congratulations.urls', namespace='congratulations')),
    # url(r'^testimonials/', include('testimonials.urls', namespace='testimonials')),
    # url(r'^structures/', include('structures.urls', namespace='structures')),
    # url(r'^certificates/', include('certificates.urls', namespace='certificates')),
    url(r'^', include('cms.urls'))
)

# This is only needed when using runserver.
if settings.DEBUG:
    urlpatterns = [
        url(r'^media/(?P<path>.*)$', serve,
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
        ] + staticfiles_urlpatterns() + urlpatterns
