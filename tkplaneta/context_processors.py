from __future__ import unicode_literals

from django.conf import settings


def debug(request):
    return {'SETTINGS_DEBUG': settings.DEBUG}
