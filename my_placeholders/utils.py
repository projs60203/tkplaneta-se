# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from my_placeholders.models import AppPlaceholder

def check_placeholder(placeholder_name):
    obj, created = AppPlaceholder.objects.get_or_create(codename=placeholder_name)
    return obj