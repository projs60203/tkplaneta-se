# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from cms.admin.placeholderadmin import PlaceholderAdminMixin

from my_placeholders.models import AppPlaceholder

class AppPlaceholderAdmin(PlaceholderAdminMixin, admin.ModelAdmin):
    list_display = ('codename', 'created_at', 'updated_at')
    readonly_fields = ('content', 'created_at', 'updated_at')
    fieldsets = (
        (
            None, {
                'fields': (
                    'codename',
                    'content',
                    'created_at',
                    'updated_at',
                )
            }
        ),
    )

admin.site.register(AppPlaceholder, AppPlaceholderAdmin)