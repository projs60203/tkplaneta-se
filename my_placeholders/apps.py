# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class MyPlaceholdersConfig(AppConfig):
    name = 'my_placeholders'
    verbose_name = _('My Placeholders')
