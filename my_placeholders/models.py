# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from cms.models.fields import PlaceholderField


def app_placeholder_slotname(instance):
    return instance.codename
    # return 'content'


@python_2_unicode_compatible
class AppPlaceholder(models.Model):
    codename = models.CharField(_('code name'), max_length=255)
    content = PlaceholderField(
        slotname=app_placeholder_slotname,
        related_name='app_placeholder',
        verbose_name=_("content")
    )
    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('updated at'), auto_now=True)

    class Meta:
        # swappable = 'AUTH_USER_MODEL'
        ordering = ['codename']
        verbose_name = _('application placeholder')
        verbose_name_plural = _('application placeholders')

    def __str__(self):
        return self.codename

