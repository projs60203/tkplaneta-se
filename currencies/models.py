# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible

from adminsortable.models import SortableMixin


@python_2_unicode_compatible
class Currency(SortableMixin):
    display_order = models.IntegerField(_('display order'), default=0)
    enabled = models.BooleanField(_('enabled?'), default=True, blank=True)

    code = models.CharField(_('code'), max_length=3, null=True, blank=True)
    digital_code = models.CharField(_('digital code'), max_length=3, null=True, blank=True)
    symbol = models.CharField(_('symbol'), max_length=1, null=True, blank=True)

    name = models.CharField(_('name'), max_length=255)

    nominative = models.CharField(_('unit nominative'), max_length=25, null=True, blank=True,
        help_text=_('Nominative, who, what?')
    )
    genitive = models.CharField(_('unit genitive'), max_length=25, null=True, blank=True,
        help_text=_('Genitive, whom, what?')
    )
    accusative = models.CharField(_('unit accusative'), max_length=25, null=True, blank=True,
        help_text=_('Accusative, whom, what?')
    )

    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('updated at'), auto_now=True)

    def __str__(self):
        return self.name

    def get_title(self, sum):
        import math
        num = math.fmod(int(sum), 10)
        if num == 1:
            return self.nominative
        elif 1 < num < 5:
            return self.genitive
        else:
            return self.accusative

    class Meta:
        ordering = ['display_order']
        verbose_name = _('currency')
        verbose_name_plural = _('currencies')
