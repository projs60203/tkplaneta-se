# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from adminsortable.admin import SortableAdmin

from currencies.models import Currency

class CurrencyAdmin(SortableAdmin):
    readonly_fields = (
        'created_at', 'updated_at'
    )
    list_display = ('name', 'symbol', 'digital_code', 'code', 'display_order', 'enabled', 'created_at')
    list_editable = ('display_order', 'enabled')
admin.site.register(Currency, CurrencyAdmin)

