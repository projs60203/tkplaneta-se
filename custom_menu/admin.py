# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from .models import CustomMenu, CustomItem


class CustomMenuAdmin(admin.ModelAdmin):
    readonly_fields = ('created_at', 'updated_at')
    list_display = ('name', 'role', 'created_at', 'updated_at')


class CustomItemAdmin(admin.ModelAdmin):
    readonly_fields = ('created_at', 'updated_at')
    list_display = ('name', 'menu', 'url', 'search_template', 'display_order', 'enabled', 'created_at', 'updated_at')
    list_editable = ('display_order', 'enabled')
    list_filter = ('menu',)

    fieldsets = (
        (_('Meta Tags'), {'fields': ('meta_title', 'meta_description', 'meta_keywords'),
                       'classes': ('collapse',)}),
        (None, {'fields': (
            'menu', 'name', 'url', 'search_template', 'display_order', 'enabled', 'created_at', 'updated_at'
        )}),
    )


admin.site.register(CustomMenu, CustomMenuAdmin)
admin.site.register(CustomItem, CustomItemAdmin)