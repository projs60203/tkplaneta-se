# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from core.utils.models.meta_tags import MetaAbstract

@python_2_unicode_compatible
class CustomMenu(models.Model):
    name = models.CharField(_('name'), max_length=255, default='')

    role = models.CharField(
        _('role'),
        max_length=50,
        blank=True
    )

    created_at = models.DateTimeField(verbose_name=_('created at'),
                                      auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('updated at'),
                                      auto_now=True)

    class Meta:
        verbose_name = _('custom menu')
        verbose_name_plural = _('custom menu list')

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class CustomItem(MetaAbstract):
    menu = models.ForeignKey(CustomMenu,
        verbose_name=_('menu'),
        related_name='items',
        on_delete=models.SET_NULL,
        null=True, blank=True
    )
    display_order = models.PositiveIntegerField(_('display order'), default=0)
    enabled = models.BooleanField(_('enabled'), default=True)
    name = models.CharField(verbose_name=_('name'), max_length=255,
                             default='')
    url = models.CharField(verbose_name=_('url'), max_length=255, blank=True,
                           default='',
                           help_text=_('example: /about/'))
    search_template = models.CharField(_('search template'), blank=True,
                                       max_length=255, default='^{}')

    created_at = models.DateTimeField(verbose_name=_('created at'),
                                      auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('updated at'),
                                      auto_now=True)

    class Meta:
        ordering = ['display_order']
        verbose_name = _('custom menu item')
        verbose_name_plural = _('custom menu items')

    def __str__(self):
        return self.name
