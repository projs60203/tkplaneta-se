# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from cms.extensions import PageExtension
from cms.extensions.extension_pool import extension_pool
from django.utils.translation import ugettext_lazy as _


class CensExtension(PageExtension):
    cens = models.BooleanField(_('censured'), default=False)


extension_pool.register(CensExtension)

