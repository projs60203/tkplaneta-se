from menus.base import Modifier
from menus.menu_pool import menu_pool
from cms.models import Page
from page_extension.models import CensExtension
from page_extension import utils_logger

class MyExampleModifier(Modifier):
    """
    This modifier makes the changed_by attribute of a page
    accessible for the menu system.
    """
    def modify(self, request, nodes, namespace, root_id, post_cut, breadcrumb):
        # only do something when the menu has already been cut
        if post_cut:
            # only consider nodes that refer to cms pages
            # and put them in a dict for efficient access
            page_nodes = {n.id: n for n in nodes if n.attr["is_page"]}
            # retrieve the attributes of interest from the relevant pages
            # pages_with_extensions_ids = []
            # extensions = CensExtension.objects.all()
            # for ext in extensions:
            #     page_we = ext.get_page()
            #     pages_with_extensions_ids.append(page_we.pk)
            # loop over all relevant pages
            # pages = Page.objects.filter(id__in=pages_with_extensions_ids)
            pages = Page.objects.filter(id__in=page_nodes.keys()).values('id', 'changed_by')
            # utils_logger.error('MyExampleModifier')
            # utils_logger.error(page)
            for page in pages:
                # take the node referring to the page
                node = page_nodes[page['id']]
                utils_logger.error('pages ids: %s '% page_nodes.keys())
                # utils_logger.error('Length: %s' % pages.__len__())
                if hasattr(page, 'censextension'):
                    # node.attr["cens"] = page.censextension.cens
                    node.attr["cens"] = page.censextension.cens
                    # utils_logger.error('page we: %s '% dir(page))
                else:
                    # node.attr["cens"] = page.censextension.cens
                    node.attr["cens"] = False
                # utils_logger.error('Node.attr %s ' % node.attr)
        return nodes

menu_pool.register_modifier(MyExampleModifier)