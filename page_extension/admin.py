# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from cms.extensions import PageExtensionAdmin

from .models import CensExtension


class IconExtensionAdmin(PageExtensionAdmin):
    pass

admin.site.register(CensExtension, IconExtensionAdmin)