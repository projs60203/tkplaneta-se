# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

def get_max_order(manager):
    try:
        return manager.order_by('-display_order').values_list('display_order', flat=True)[0]
    except IndexError:
        return 0


class OrderedModelManager(models.Manager):
    def swap(self, obj1, obj2):
        """
        Swap places in order of two objects.
        If some of the objects is empty (mostly obj2) then do nothing
        """
        if not (obj1 and obj2):
            return
        if not (isinstance(obj1, self.model) and isinstance(obj2, self.model)):
            raise TypeError("%r and %r must be instances of %r" %
                            (obj1, obj2, self.model))
        obj1.display_order, obj2.display_order = obj2.display_order, obj1.display_order
        obj1.save()
        obj2.save()

    def max_order(self):
        return get_max_order(self)

    def fix_ordering(self):
        """
        This method must be executed only if this application is
        added to existing project.
        """
        for index, item in enumerate(self.only('display_order'), 1):
            item.display_order = index
            item.save()


class OrderedModel(models.Model):
    display_order = models.PositiveIntegerField(blank=True, default=1, db_index=True)

    objects = OrderedModelManager()

    class Meta:
        abstract = True
        ordering = ['display_order']

    def save(self, *args, **kwargs):
        if not self.id:
            self.display_order = get_max_order(self.__class__.objects) + 1
        super(OrderedModel, self).save(*args, **kwargs)

    def move_down(self):
        self.swap(self.get_next_by_order())

    def move_up(self):
        self.swap(self.get_previous_by_order())

    def swap(self, obj):
        """
        Swap places in display_order of two objects.
        If some of the objects is empty (mostly obj2) then do nothing
        """
        if not (obj):
            return
        if not (isinstance(obj, self.__class__)):
            raise TypeError("%r must be instances of %r" %
                            (obj, self.__class__))
        self.display_order, obj.display_order = obj.display_order, self.display_order
        self.save()
        obj.save()

    def get_next_by_order(self):
        try:
            return self.__class__.objects.filter(display_order__gt=self.display_order).order_by('display_order')[0]
        except IndexError:  # Last item
            return

    def get_previous_by_order(self):
        try:
            return self.__class__.objects.filter(display_order__lt=self.display_order).order_by('-display_order')[0]
        except IndexError:  # First item
            return
