# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django.utils.translation import pgettext, ugettext_lazy as _


class ContactForm(forms.Form):
    """
    Contact form
    """
    name = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'inputs',
                'placeholder': pgettext("contact form", _('Name')),
                'tabindex': '1'
            }
        ),
        label=pgettext("contact form", _('Name')),
        required= False,
        # error_messages={
            # "required": _("Please input name"),
        # }
    )
    email = forms.EmailField(
        widget=forms.TextInput(
            attrs={
                'class': 'inputs',
                'placeholder': _("Email Address"),
                'tabindex': '2'
            }
        ),
        label=_("Email Address"),
        required= True,
        error_messages={
            "required": _("Enter email, please"),
        }
    )
    # phone = forms.CharField(
    #     widget=forms.TextInput(
    #         attrs={
    #             'class': 'inputs',
    #             'placeholder': _('Phone'),
    #             'tabindex': '3'
    #         }
    #     ),
    #     label=_("Phone"),
    #     required= False,
        # error_messages={
        #     "required": _("Enter Phone or email, please"),
        # }
    # )
    message = forms.CharField(
        widget=forms.Textarea(
            attrs={
                'class': 'textareas',
                'placeholder': _('Message'),
                'tabindex': '4'
            }
        ),
        label=_("Message"),
        error_messages={
            "required": _("Please input message"),
        }
    )

    def clean(self):
        """
        Validate that the supplied email address or phone is present.
        """
        cleaned_data = super(ContactForm, self).clean()
        email = cleaned_data.get('email')
        # phone = cleaned_data.get('phone')
        if email: # or phone:
            pass
        else:
            error_list=self.errors.get('email')
            if error_list is None:
                 error_list=forms.utils.ErrorList()
                 self.errors['email']=error_list
            error_list.append(self.fields['email'].error_messages["required"])
        return cleaned_data
