# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url
from django.conf import settings

from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _
from django.conf.urls import url

from cms.views import details
from .views import congratulation_order, congratulation_thank_you

class CongratulationsApp(CMSApp):
    app_name = 'congratulations'
    name = _('Congratulations')

    def get_urls(self, page=None, language=None, **kwargs):
        urlpatterns = [
            url(r'^$', congratulation_order, name='congratulation_order'),
            url(r'^(?P<slug>[0-9A-Za-z-_.//]+)$', details, name='congratulation_thank_you')
        ]
        return urlpatterns

    permissions = False

apphook_pool.register(CongratulationsApp)