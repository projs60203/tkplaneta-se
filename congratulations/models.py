# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from filer.fields.image import FilerImageField
from easy_thumbnails.files import get_thumbnailer


class CONGRATULATION_STATUS:
    NEW = 'new'
    IN_WORK = 'in_work'
    PAID = 'paid'
    MADE = 'made'
    RENOUNCE = 'renounce'
    BLACK_LIST = 'black_list'

    CHOICES = (
        (NEW, _('new')),
        (IN_WORK, _('in work')),
        (PAID, _('paid')),
        (MADE, _('made')),
        (RENOUNCE, _('renounce')),
        (BLACK_LIST, _('black_list'))
    )


class CONGRATULATION_PAYMENT_TYPE:
    CASH = 'cash'
    CREDIT_CARD = 'credit_card'

    CHOICES = (
        (CASH, _('Cash')),
        (CREDIT_CARD, _('Credit card')),
    )



@python_2_unicode_compatible
class Congratulation(models.Model):
    created_at = models.DateTimeField(_('created at'), auto_now_add=True)

    sender_name = models.TextField(_('sender name'))
    phone = models.CharField(_('phone'), max_length=255, blank=True, default='')
    email = models.EmailField(_('email'), blank=True, default='')

    display_date = models.DateField(_('display date'))
    recipient_name = models.CharField(_('recipient name'), max_length=255)
    recipient_address = models.CharField(_('recipient address'), max_length=255, blank=True, null=True)

    congratulator = models.TextField(_('congratulator'))

    wish = models.TextField(_('wish'))

    payment = models.TextField(_('payment'), blank=True, null=True)
    order_number = models.CharField(_('order number'), max_length=255, blank=True, null=True)

    payment_type = models.CharField(_('payment type'), max_length=255,
        choices=CONGRATULATION_PAYMENT_TYPE.CHOICES, default=CONGRATULATION_PAYMENT_TYPE.CASH)
    price = models.FloatField(_('price'), blank=True, default=0.0)

    status = models.CharField(_('status'), max_length=255,
        choices=CONGRATULATION_STATUS.CHOICES, default=CONGRATULATION_STATUS.NEW)

    uuid_name = models.CharField(_('uuid name'), max_length=255, blank=True, default='')

    def __str__(self):
        return self.sender_name

    class Meta:
        verbose_name = _('congratulation')
        verbose_name_plural = _('congratulations')


@python_2_unicode_compatible
class MusicVariant(models.Model):
    congratulation = models.ForeignKey(
        Congratulation, verbose_name=_('congratulation'), related_name='music_variants'
    )
    artist = models.ForeignKey('artists.Artist', verbose_name=_('artist'), related_name='music_variants',
        on_delete=models.DO_NOTHING)
    composition = models.ForeignKey('artists.Composition', verbose_name=_('composition'), related_name='music_variants',
        on_delete=models.DO_NOTHING)

    def __str__(self):
        return u'%s, %s' % (self.artist, self.composition)

    def save(self, *args, **kwargs):
        if self.composition:
            artist_id = self.composition.artist_id
            if self.artist_id == artist_id:
                pass
            else:
                self.artist_id = artist_id
        super(MusicVariant, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _('music variant')
        verbose_name_plural = _('music variants')


class Photo(models.Model):
    congratulation = models.ForeignKey(
        Congratulation, verbose_name=_('congratulation'), related_name='photos'
    )
    image = FilerImageField(
        verbose_name=_('image'),
        on_delete=models.DO_NOTHING,
    )

    class Meta:
        verbose_name = _('photo')
        verbose_name_plural = _('photos')

    def admin_thumbnail(self):
        if self.image:
            thumb_url = get_thumbnailer(self.image).get_thumbnail({'size': (50, 64), 'quality': 100}).url
            return '<img src="{}" />'.format(thumb_url)
        else:
            return ''
    admin_thumbnail.short_description = _('image')
    admin_thumbnail.allow_tags = True
