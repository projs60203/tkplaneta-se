# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django.utils.translation import pgettext, ugettext_lazy as _
from django.forms.widgets import ClearableFileInput
from django.conf import settings
from django.utils.text import slugify

from dal import autocomplete

from core.configs.models import AppConfig
from congratulations.models import Congratulation, MusicVariant, Photo


class MusicVariantAdminForm(forms.ModelForm):
    class Meta:
        model = MusicVariant
        fields = (
            'artist', 'composition'
        )
        widgets = {
            'artist': autocomplete.ModelSelect2(url='artist-autocomplete'),
            'composition': autocomplete.ModelSelect2(url='composition-autocomplete', forward=['artist'])
        }


class CongratulationForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CongratulationForm, self).__init__(*args, **kwargs)

        maxlength = AppConfig.get_config(key='wish_maxlength', default=300)

        self.fields['sender_name'].error_messages['required'] = _('Enter name, please')
        self.fields['phone'].required = False
        self.fields['phone'].error_messages['required'] = _('Enter Phone or email, please')

        self.fields['recipient_name'].error_messages['required'] = _('Enter recipient name, please')
        # self.fields['recipient_address'].error_messages['required'] = _('Enter recipient address, please')
        self.fields['congratulator'].error_messages['required'] = _('Enter congratulator, please')
        self.fields['wish'].error_messages['required'] = _('Enter wish, please')
        self.fields['wish'].widget.attrs['maxlength'] = maxlength

    class Meta:
        model = Congratulation
        fields = (
            'sender_name', 'phone', 'email',
            'display_date', 'recipient_name', 'recipient_address',
            'congratulator', 'wish',
        )
        widgets = {
            'sender_name': forms.TextInput(
                attrs={
                    'class': 'inputs',
                    'placeholder': _('Full Name'),
                    'tabindex': '1'
                }
            ),
            'email': forms.TextInput(
                attrs={
                    'class': 'inputs',
                    'placeholder': _('Email'),
                    'tabindex': '2'
                }
            ),
            'phone': forms.TextInput(
                attrs={
                    'class': 'inputs',
                    'placeholder': _('Phone'),
                    'tabindex': '3'
                }
            ),
            'display_date': forms.TextInput(
                attrs={
                    'class': 'datepicker inputs',
                    'placeholder': _('Date'),
                    'form': 'congratulation_form_id'
                }
            ),
            'recipient_name': forms.TextInput(
                attrs={
                    'class': 'inputs',
                    'placeholder': _('Full Name'),
                    'form': 'congratulation_form_id'
                }
            ),
            'recipient_address': forms.TextInput(
                attrs={
                    'class': 'inputs',
                    'placeholder': "",
                    'form': 'congratulation_form_id'
                }
            ),
            'congratulator': forms.Textarea(
                attrs={
                    'class': 'textarea_popup',
                    'placeholder': "",
                    'rows': '2',
                    'form': 'congratulation_form_id'
                }
            ),
            'wish': forms.Textarea(
                attrs={
                    'class': 'textarea_popup2 limited',
                    'placeholder': "",
                    'rows': '6',
                    'form': 'congratulation_form_id'
                }
            ),
        }

    def clean(self):
        """
        Validate that the supplied email address or phone is present.
        """
        cleaned_data = super(CongratulationForm, self).clean()
        email = cleaned_data.get('email')
        phone = cleaned_data.get('phone')
        if email or phone:
            pass
        else:
            error_list=self.errors.get('phone')
            if error_list is None:
                 error_list=forms.utils.ErrorList()
                 self.errors['phone']=error_list
            error_list.append(self.fields['phone'].error_messages["required"])
        return cleaned_data


class MusicVariantForm(forms.ModelForm):
    class Meta:
        model = MusicVariant
        fields = (
            'artist', 'composition'
        )
        widgets = {
            'artist': autocomplete.ModelSelect2(
                url='artist-autocomplete', forward=['composition'],
                attrs={
                    'data-placeholder': _('Select Artist'),
                    'class': 'artist-select',
                    # 'data-minimum-input-length': 2,
                },
            ),
            'composition': autocomplete.ModelSelect2(
                url='composition-autocomplete', forward=['artist'],
                attrs={
                    'data-placeholder': _('Select Composition'),
                    'class': 'composition-select',
                    # 'data-minimum-input-length': 2,
                },
            )
        }

    def __init__(self, *args, **kwargs):
        super(MusicVariantForm, self).__init__(*args, **kwargs)
        self.fields['artist'].empty_label = ''
        self.fields['composition'].empty_label = ''


class PhotoVariantForm(forms.Form):
    image = forms.ImageField(
        label=_('Photo'),
        widget=ClearableFileInput(
            attrs={'class': '_image-field'}
        )
    )
