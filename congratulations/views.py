# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
import json
import uuid

from django.shortcuts import redirect, render
from django.http import HttpResponse, JsonResponse
from django.contrib.sites.models import Site
from django.conf import settings
from django.core.urlresolvers import reverse
from django.forms.models import inlineformset_factory
from django.forms import formset_factory
from django.core.files import File
from django.utils.translation import ugettext_lazy as _

from congratulations.models import Congratulation, MusicVariant, Photo
from congratulations.forms import CongratulationForm, MusicVariantForm, PhotoVariantForm
from core.configs.models import AppConfig
from core.utils.mail.send_mail import send_mail, send_mail2
from core.utils.forms.utils import form_errors_to_json
from filer.utils.loader import load_model
from core.utils import get_file_name

CONGRATULATIONS_THANK_YOU = 'congratulations_thank_you'

def congratulation_order(request):
    from cms.models.pagemodel import Page
    template = 'pages/congratulation.html'

    # MusicVariant
    music_variant_formset_class = inlineformset_factory(
        parent_model=Congratulation,
        model=MusicVariant,
        form=MusicVariantForm,
        can_delete=True,
        max_num=3,
        validate_max=True,
        extra=1,
    )
    music_variant_formset = music_variant_formset_class(
        request.POST or None,
        prefix='music_variant',
        # instance=resume
    )
    # Photo
    photo_variant_formset_class = formset_factory(
        form=PhotoVariantForm,
        can_delete=False,
        extra=5,
    )
    photo_variant_formset = photo_variant_formset_class(
        request.POST or None,
        request.FILES or None,
        prefix='photo_variant',
    )

    if request.method == 'POST':
        form = CongratulationForm(request.POST)
        invalid_forms = []
        if form.is_valid():
            pass
        else:
            invalid_forms.append(form)

        if music_variant_formset.is_valid():
            pass
        else:
            invalid_forms.append(music_variant_formset)
        if photo_variant_formset.is_valid():
            pass
        else:
            invalid_forms.append(photo_variant_formset)
        if invalid_forms:
            if request.is_ajax():
                return HttpResponse(form_errors_to_json(invalid_forms), content_type="application/json")
        else:
            congratulation = form.save(commit=False)
            congratulation_price = AppConfig.get_config('congratulation_price', 0)
            congratulation.price = float(congratulation_price)
            congratulation.uuid_name = uuid.uuid4()
            congratulation.save()

            for frm in music_variant_formset:
                cd = frm.cleaned_data
                artist = cd.get('artist')
                composition = cd.get('composition')
                if artist and composition:
                    music_variant = frm.save(commit=False)
                    music_variant.congratulation = congratulation
                    music_variant.save()

            ImageModel = load_model(settings.FILER_IMAGE_MODEL)
            for frm in photo_variant_formset:
                if frm.cleaned_data.get('image'):
                    file_name = get_file_name(frm.cleaned_data.get('image').name)
                    file_save_dir = os.path.join(settings.MEDIA_ROOT, 'congratulations', 'photos')
                    # Check if dir present
                    try:
                        os.makedirs(file_save_dir)
                    except OSError as e:
                        if not os.path.isdir(file_save_dir):
                            raise
                    full_path = os.path.join(file_save_dir, file_name)
                    if os.path.exists(full_path):
                        os.remove(full_path)
                    try:
                        with open(full_path, 'wb') as f:
                            for chunk in frm.cleaned_data.get('image').chunks():
                                f.write(chunk)
                            f.close()
                        img = File(open(full_path, 'r'))
                        _image, created = ImageModel.objects.get_or_create(file=img, defaults={
                            'name': file_name,
                            'description': 'Congratulation Photo',
                        })
                        photo = Photo(congratulation=congratulation)
                        photo.image = _image
                        photo.save()
                    except Exception:
                        raise
            context = {
                'site': Site.objects.get_current(),
                'object': congratulation,
            }
            email = AppConfig.get_config('congratulation_form_email', settings.DEFAULT_FROM_EMAIL)

            send_mail(
                recipients=email,
                subject_template='congratulations/mail/congratulation_notify_subject.txt',
                html_template='congratulations/mail/congratulation_notify.html',
                text_template='congratulations/mail/congratulation_notify.txt',
                context=context,
            )
            if congratulation.email:
                subject = AppConfig.get_config('congratulation_mail_subject', _('Default mail subject'))
                send_mail2(
                    recipients=congratulation.email,
                    subject=subject,
                    html_template='congratulations/mail/congratulation.html',
                    text_template='congratulations/mail/congratulation.txt',
                    context=context,
                )
            # detect redirect url
            cms_pages = Page.objects.filter(reverse_id=CONGRATULATIONS_THANK_YOU)
            if cms_pages:
                redirect_to = cms_pages[0].get_absolute_url()
            else:
                redirect_to = reverse('congratulations:congratulation_thank_you')
            if request.is_ajax():
                to_json = {
                    'success': True,
                    'location': redirect_to
                }
                return HttpResponse(json.dumps(to_json), content_type="application/json")
            else:
                return redirect(redirect_to)
    else:
        form = CongratulationForm()

    ctx = {
        'form': form,
        'music_variant_formset': music_variant_formset,
        'photo_variant_formset': photo_variant_formset,
    }
    return render(request, template, ctx)


def congratulation_thank_you(request):
    from my_placeholders.utils import check_placeholder
    template = 'pages/thank_you.html'
    content_placeholder= check_placeholder(CONGRATULATIONS_THANK_YOU)
    ctx = {
        'content_placeholder': content_placeholder,
    }
    return render(request, template, ctx)
