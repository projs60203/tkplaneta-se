# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from congratulations.models import Congratulation, MusicVariant, Photo
from congratulations.forms import MusicVariantAdminForm


class MusicVariantInline(admin.TabularInline):
    model = MusicVariant
    form = MusicVariantAdminForm
    extra = 0


class PhotoInline(admin.TabularInline):
    model = Photo
    extra = 0


class CongratulationAdmin(admin.ModelAdmin):
    list_display = ('sender_name', 'status', 'created_at', 'display_date')
    list_filter = ('status', 'created_at', 'display_date')
    readonly_fields = ('created_at', 'uuid_name')
    fieldsets = (
        (
            None, {
                'fields': (
                    ('display_date', 'sender_name'),
                    ('recipient_name', 'phone'),
                    ('recipient_address', 'email'),
                    'congratulator',
                    'wish',
                    ('status', 'order_number'),
                    ('payment_type', 'price'),
                    'payment',
                    ('uuid_name', 'created_at'),
                )
            }
        ),
    )
    inlines = [MusicVariantInline, PhotoInline]


admin.site.register(Congratulation, CongratulationAdmin)
