# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url
from django.conf import settings
from django.contrib import admin

from cms.views import details

from .views import congratulation_order, congratulation_thank_you

admin.autodiscover()
urlpatterns = [
    url(r'^$', congratulation_order, name='congratulation_order'),
    url(r'^(?P<slug>[0-9A-Za-z-_.//]+)$', details, name='congratulation_thank_you')
]