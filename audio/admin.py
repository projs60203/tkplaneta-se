# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.contenttypes.admin import GenericStackedInline

from audio.forms import AudioAdminForm
from audio.models import Audio


class AudioInline(GenericStackedInline):
    model = Audio
    extra = 1


class AudioAdmin(admin.ModelAdmin):
    list_display = ('name', 'position', 'enabled', 'updated_at')
    list_editable = ('position', 'enabled')

    list_filter    = ('created_at', 'updated_at',)

    readonly_fields = ('created_at', 'updated_at', )
    search_fields  = ('name', 'description',)

    form = AudioAdminForm

    def queryset(self, request):
        qs = super(AudioAdmin, self).queryset(request)
        return qs.filter(content_type__isnull=True, object_id__isnull=True)

    # def get_form(self, request, obj=None, **kwargs):
    #   form = super(AudioAdmin, self).get_form(request, obj, **kwargs)
    #   del form.base_fields['content_type']
    #   del form.base_fields['object_id']
    #   return form

admin.site.register(Audio, AudioAdmin)
