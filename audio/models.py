# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.contenttypes import fields
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.models import ContentType

from filer.fields.file import FilerFileField


@python_2_unicode_compatible
class Audio(models.Model):
    content_type = models.ForeignKey(ContentType, null=True)
    object_id = models.PositiveIntegerField(null=True,
        db_index=True
    )
    content_object = fields.GenericForeignKey(ct_field='content_type', fk_field='object_id')

    position = models.PositiveIntegerField(_('position'), default=0)

    enabled = models.BooleanField(_('enabled?'), default=True, blank=True)
    name = models.CharField(_('name '), max_length=255, blank=True, default='')
    description = models.TextField(_('description'), blank=True, default='')

    # sound formats
    track_aac = FilerFileField(
        verbose_name=_('aac format track'),
        on_delete = models.DO_NOTHING,
        related_name = 'audio_aac_files',

    )
    track_ogg = FilerFileField(
        verbose_name=_('ogg format track'),
        on_delete=models.DO_NOTHING,
        related_name='audio_ogg_files',
    )
    track_wav = FilerFileField(
        verbose_name=_('wav format track'),
        on_delete=models.DO_NOTHING,
        related_name='audio_wav_files',
    )

    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('updated at'), auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['position']
        verbose_name = _('audio')
        verbose_name_plural = _('audio')

