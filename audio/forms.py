# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django.utils.translation import pgettext, ugettext_lazy as _

from audio.models import Audio

class AudioAdminForm(forms.ModelForm):
    class Meta:
        model = Audio
        exclude = ('content_type', 'object_id')
        # fields = (
        #     'position', 'enabled', 'name', 'description',
        #     'track_aac', 'track_ogg', 'track_wav'
        # )
