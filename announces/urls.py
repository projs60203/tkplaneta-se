# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url
from django.contrib import admin

from .views import announce_order, announce_thank_you

admin.autodiscover()

urlpatterns = [
    url(r'^$', announce_order, name='announce_order'),
    url(r'^thank-you/$', announce_thank_you, name='announce_thank_you'),
]
