# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


class AnnouncesApp(CMSApp):
    app_name = 'announces'
    name = _('Announces')

    def get_urls(self, page=None, language=None, **kwargs):
        return ['announces.urls']

apphook_pool.register(AnnouncesApp)