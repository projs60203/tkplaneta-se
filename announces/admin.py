# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from announces.models import Announce


class AnnounceAdmin(admin.ModelAdmin):
    list_display = ('name', 'status', 'created_at',)
    readonly_fields = ('created_at',)
    list_filter = ('status', 'created_at',)
    fieldsets = (
        (
            None, {
                'fields': (
                    'created_at',
                    'name',
                    'phone',
                    'email',
                    'text',
                    'payment',
                    'order_number',
                    'payment_type',
                    'price',
                    'status',
                )
            }
        ),
    )
admin.site.register(Announce, AnnounceAdmin)
