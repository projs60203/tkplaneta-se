# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
import json
import uuid

from django.shortcuts import redirect, render
from django.http import HttpResponse, JsonResponse
from django.contrib.sites.models import Site
from django.conf import settings
from django.core.urlresolvers import reverse
from django.forms.models import inlineformset_factory
from django.forms import formset_factory
from django.core.files import File
from django.utils.translation import ugettext_lazy as _

from certificates.models import Certificate
from certificates.forms import CertificateForm
from core.configs.models import AppConfig
from core.utils.mail.send_mail import send_mail,send_mail2
from core.utils.forms.utils import form_errors_to_json
from filer.utils.loader import load_model

from announces.models import Announce
from announces.forms import AnnounceForm

ANNOUNCES_THANK_YOU = 'announces_thank_you'

def announce_order(request):
    from cms.models.pagemodel import Page
    ctx = {
    }
    template = 'pages/announce.html'
    if request.method == 'POST':
        form = AnnounceForm(request.POST)

        if form.is_valid():
            announce = form.save(commit=False)

            announce_price = AppConfig.get_config('announce_price', 0)
            announce.price = float(announce_price)
            announce.uuid_name = uuid.uuid4()
            announce.save()

            context = {
                'site': Site.objects.get_current(),
                'object': announce,
            }

            email = AppConfig.get_config('announce_form_email', settings.DEFAULT_FROM_EMAIL)

            send_mail(
                recipients=email,
                subject_template='announces/mail/announcement_notify_subject.txt',
                html_template='announces/mail/announcement_notify.html',
                text_template='announces/mail/announcement_notify.txt',
                context=context,
            )

            if announce.email:
                subject = AppConfig.get_config('announce_mail_subject', _('Default mail subject'))
                send_mail2(
                    recipients=announce.email,
                    subject=subject,
                    html_template='announces/mail/announcement.html',
                    text_template='announces/mail/announcement.txt',
                    context=context,
                )
            # detect redirect url
            cms_pages = Page.objects.filter(reverse_id=ANNOUNCES_THANK_YOU)
            if cms_pages:
                redirect_to = cms_pages[0].get_absolute_url()
            else:
                redirect_to = reverse('announces:announce_thank_you')
            if request.is_ajax():
                to_json = {
                    'success': True,
                    'location': redirect_to
                }
                return HttpResponse(json.dumps(to_json), content_type="application/json")
            else:
                return redirect(redirect_to)
        else:
            if request.is_ajax():
                return HttpResponse(form_errors_to_json(form), content_type="application/json")

    form = AnnounceForm()
    ctx['form'] = form
    return render(request, template, ctx)


def announce_thank_you(request):
    from my_placeholders.utils import check_placeholder
    template = 'thank_you.html'
    content_placeholder= check_placeholder(ANNOUNCES_THANK_YOU)
    ctx = {
        'content_placeholder': content_placeholder,
    }
    return render(request, template, ctx)

