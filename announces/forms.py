# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django.utils.translation import pgettext, ugettext_lazy as _
from django.forms.widgets import ClearableFileInput
from django.conf import settings
from django.utils.text import slugify

from dal import autocomplete

from core.configs.models import AppConfig
from announces.models import Announce


class AnnounceForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(AnnounceForm, self).__init__(*args, **kwargs)
        self.fields['name'].error_messages['required'] = _('Enter name, please')
        self.fields['phone'].required = False
        self.fields['phone'].error_messages['required'] = _('Enter Phone or email, please')
        self.fields['text'].error_messages['required'] = _('Enter announcement text, please')

    class Meta:
        model = Announce
        fields = (
            'name', 'phone', 'email', 'text'
        )
        widgets = {
            'name': forms.TextInput(attrs={'class': 'inputs', 'placeholder': _('Full Name')}),
            'phone': forms.TextInput(attrs={'class': 'inputs', 'placeholder': _('Phone')}),
            'email': forms.TextInput(attrs={'class': 'inputs', 'placeholder': _('Email')}),
            'text': forms.Textarea(attrs={'class': 'textareas limited', 'placeholder': _('Announce text:')}),
        }

    def clean(self):
        """
        Validate that the supplied email address or phone is present.
        """
        cleaned_data = super(AnnounceForm, self).clean()
        email = cleaned_data.get('email')
        phone = cleaned_data.get('phone')
        if email or phone:
            pass
        else:
            error_list=self.errors.get('phone')
            if error_list is None:
                 error_list=forms.utils.ErrorList()
                 self.errors['phone']=error_list
            error_list.append(self.fields['phone'].error_messages["required"])
        return cleaned_data

